<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Restriction extends Model
{
    use SoftDeletes;

    protected $table = 'restrictions';
    
    protected $fillable = [
        'name'
    ];
}
