<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AccessControlProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->composeSidebar();
    }

    /**
     * @return void
     */
    public function composeSidebar()
    {
        view()->composer(
            'layouts.sidebar',
            'App\Http\ViewComposers\SidebarComposer'
        );
    }
}
