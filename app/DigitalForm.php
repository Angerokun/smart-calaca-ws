<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DigitalForm extends Model
{
    use SoftDeletes;

    protected $table = 'digital_forms';

    protected $fillable = [
        'name',
        'url',
        'file'
    ];
}
