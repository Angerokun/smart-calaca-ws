<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormRequirement extends Model
{
    use SoftDeletes;

    protected $table = "form_requirement_items";

    protected $fillable = [
        'form_id',
        'file_id',
        'name',
        'description'
    ];

    /**
     * Form relationship
     */
    public function form(): BelongsTo
    {
        return $this->belongsTo(Form::class, 'form_id');
    }

    /**
     * Form relationship
     */
    public function digitalForm(): BelongsTo
    {
        return $this->belongsTo(DigitalForm::class, 'file_id');
    }
}
