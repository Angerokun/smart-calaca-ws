<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use SoftDeletes;

    protected $table = 'departments';

    protected $fillable = [
        'name',
        'description'
    ];

    /**
     * designation relationship
     *
     * @return HasMany
     */
    public function designations(): HasMany
    {
        return $this->hasMany(Designation::class, 'department_id');
    }

    /**
     * hotline relationship
     *
     * @return HasOne
     */
    public function hotline(): HasOne
    {
        return $this->hasOne(DepartmentHotline::class, 'department_id');
    }
}
