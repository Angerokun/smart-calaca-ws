<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EmergencyRequest extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'department_id' => $this->department_id,
            'area_incident' => $this->area_incident,
            'type_incident' => $this->type_incident,
            'remarks' => $this->remarks,
            'created_at' => $this->created_at,
            'user_info' => $this->user,
            'department_info' => $this->department
        ];
    }
}
