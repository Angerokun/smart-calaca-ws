<?php

namespace App\Http\Resources;

use App\Http\Resources\FormRequirement as FormRequirementResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Form extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'department_id' => $this->department_id,
            'name' => $this->name,
            'description' => $this->description,
            'department_info' => $this->department,
            'requirements' => FormRequirementResource::collection($this->formRequirements)
        ];
    }
}
