<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdminRequestActivity extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'requested_id' => $this->requested_id,
            'assigned_to' => $this->assigned_to,
            'status' => $this->status,
            'remarks' => $this->remarks,
            'file_name' => $this->file_name,
            'file' => $this->file,
            'update_user' => $this->update_user,
            'update_info' => $this->update_info,
            'date' => $this->created_at
        ];
    }
}
