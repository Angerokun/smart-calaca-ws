<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RequestorDocument extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'requested_id' => $this->requested_id,
            'user_id' => $this->user_id,
            'remarks' => $this->remarks,
            'file_name' => $this->file_name,
            'file' => $this->file
        ];
    }
}
