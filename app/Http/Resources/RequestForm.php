<?php

namespace App\Http\Resources;

use App\Http\Resources\AdminRequestActivity;
use App\Http\Resources\Form;
use Illuminate\Http\Resources\Json\JsonResource;

class RequestForm extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'requested_no' => $this->requested_no,
            'user_id' => $this->user_id,
            'form_id' => $this->form_id,
            'priority' => $this->priority,
            'description' => $this->description,
            'status' => $this->status,
            'form_info' => new Form($this->form),
            'user_info' => $this->user,
            'documents' => $this->documents,
            'activities' => AdminRequestActivity::collection($this->activities()->orderByActivities()->get()),
            'request_date' => date('F d, Y', strtotime($this->created_at))
        ];
    }
}
