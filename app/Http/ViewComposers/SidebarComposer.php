<?php

namespace App\Http\ViewComposers;

use App\Module;
use App\ModulePermission;
use App\UserAccess;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class SidebarComposer
{
    /**
     * compos
     *
     * @param View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $modules = $this->modulesPermission();
        
        $view->with('modules', $modules);
    }

    /**
     * module permission
     *
     * @return void
     */
    public function modulesPermission()
    {
        $sidebar = '';

        if (Auth::User()->id == 1) {
            $modules = Module::all();
            foreach ($modules as $key => $module) {
                if ($module->module_id == null) {
                    $subModules = Module::where('module_id', $module->id)->get();
                    if (count($subModules) > 0) {
                        $sidebar .= '<hr class="sidebar-divider">
                                        <div class="sidebar-heading">' . $module->name . '</div>';
                        foreach ($subModules as $key => $sub_module) {
                            $sidebar .= '<li class="nav-item">
                                            <a class="nav-link" href="' . route($sub_module->route) . '">
                                                <i class="' . $sub_module->icon . '"></i>
                                                <span>' . $sub_module->name . '</span></a>
                                        </li>';
                        }
                    } else {
                        // $sidebar .= '<li><a href="' . route($module->route) . '" class="active">'
                        //             . '<i class="' . $module->icon . '"></i> <span>' . $module->name . '</span></a>'
                        //         . '</li>';
                    }
                }
            }
        } else {
            $userAccess = UserAccess::where('user_id', Auth::User()->id)->first();
            $add_parent = array();
            foreach ($userAccess->userPermission->modulePermission as $key => $user_module) {
                $modules = Module::all();
                
                foreach ($modules as $key => $module) {
                    if ($module->module_id == null) {
                        $with_parent = false;
                        $subModules = Module::where('module_id', $module->id)->get();
                        $subModuleIds = array();
                        foreach ($subModules as $key => $sub_module) {
                            $subModuleIds[] = $sub_module->id;
                        }

                        if (count($subModuleIds) > 0) {
                            $check_with_parent = ModulePermission::where('permission_id', $userAccess->permission_id)
                                ->whereIn('module_id', $subModuleIds)->get();
                            if (count($check_with_parent) > 0) {
                                $with_parent = true;
                            }
                        }
                    }
                    if ($user_module->module_id == $module->id || $with_parent == true) {
                        if ($module->module_id == null && !in_array($module->id, $add_parent)) {
                            $subModules = Module::where('module_id', $module->id)->get();
                            if (count($subModules) > 0) {
                                $add_parent[] = $module->id;
                                $sidebar .= '<hr class="sidebar-divider">
                                        <div class="sidebar-heading">' . $module->name . '</div>';
                                foreach ($subModules as $key => $sub_module) {
                                    $check_sub_modules = ModulePermission::where('permission_id', $userAccess->permission_id)
                                        ->where('module_id', $sub_module->id)->first();
                                    if ($check_sub_modules) {
                                        $sidebar .= '<li class="nav-item">
                                            <a class="nav-link" href="' . route($sub_module->route) . '">
                                                <i class="' . $sub_module->icon . '"></i>
                                                <span>' . $sub_module->name . '</span></a>
                                        </li>';
                                    }
                                }
                            } else {
                                // $sidebar .= '<li><a href="' . route($module->route) . '" class="">'
                                //             . '<i class="' . $module->icon . '"></i> <span>' . $module->name . '</span></a>'
                                //         . '</li>';
                            }
                        }
                    }
                }
            }
        }

        return $sidebar;
    }
}
