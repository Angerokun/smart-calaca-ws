<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class NotificationController extends Controller
{
    /**
     * Get unread Notification
     *
     * @return void
     */
    public function getUnreadNotificationCount()
    {
        return Auth::User()->unreadNotifications->count();
    }
    
    /**
     * Get read all Notifications
     *
     * @return void
     */
    public function readAllNotification()
    {
        return Auth::User()->unreadNotifications->markAsRead();
    }
    

    public function markAsViewNotification(string $id)
    {
        $notification = Auth::User()->notifications->where('id', $id)->first();
        return $notification->markAsView();
    }

    public function index()
    {
        return view('notification.index');
    }

    /**
     * datatables
     *
     * @return void
     */
    public function notificationDatatable()
    {
        $notification = Auth::User()->notifications;

        $dt = DataTables::of($notification)
        ->addColumn('route', function ($model) {
            return $model->data['route'];
        })
        ->addColumn('date', function ($model) {
            return $model->created_at;
        })
        ->addColumn('description', function ($model) {
            return $model->data['created_at'] . ' ' . $model->data['other_info'];
        })->make(true);

        return $dt;
    }
}
