<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class DigitalFormController extends Controller
{
     /**
     * birth certificate registration
     *
     * @return void
     */
    public function birthCertificate($id)
    {   
        
        $user = User::findOrFail($id);
        return view(
            'digital-forms.birth-certificate.create',
            [ 'requestor' => $user->user]
        );
    }
}
