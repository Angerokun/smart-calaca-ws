<?php

namespace App\Http\Controllers;

use App\Module;
use App\RequestForm;
use App\RequestorDocument;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Response;

use function App\Common\modulePermission;
use function App\Datatable\action_data;

class RequestFormController extends Controller
{
    private $module;
    /**
     * construct
     */
    public function __construct()
    {
        $this->module = Module::where('module_name', 'request_forms')->first();
    }

    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        return view(
            'request-form.index'
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * show
     *
     * @param int $requestId
     *
     * @return void
     */
    public function show(int $requestId)
    {
        $requestForm = RequestForm::findOrFail($requestId);
        $percent = 0;
        if ($requestForm->status == 'assigned') {
            $percent = 20;
        } elseif ($requestForm->status == 'in_progress') {
            $percent = 50;
        } elseif ($requestForm->status == 'pending') {
            $percent = 70;
        } elseif ($requestForm->status == 'approved' || $requestForm->status == 'approved') {
            $percent = 100;
        }


        return view(
            'request-form.show',
            [
                'requestForm' => $requestForm,
                'percent' => $percent
            ]
        );
    }
    
    /**
     * delete
     *
     * @param int $requestId
     *
     * @return void
     */
    public function delete(int $requestId)
    {
        $requestForm = RequestForm::findOrFail($requestId);

        return view(
            'request-form.delete',
            ['requestForm' => $requestForm]
        );
    }

    /**
     * destroy
     *
     * @param int $deptId
     *
     * @return void
     */
    public function destroy(int $requestId)
    {
        $requestForm = RequestForm::findOrFail($requestId);

        $requestForm->delete();

        return redirect(route('request-forms.index'));
    }

    /**
     * datatables
     *
     * @return void
     */
    public function requestFormDatatable()
    {
        $requestForms = RequestForm::all();

        $dt = DataTables::of($requestForms)
        ->addColumn('id', function ($model) {
            return $model->id;
        })
        ->addColumn('date_created', function ($model) {
            return $model->created_at;
        })
        ->addColumn('requested_no', function ($model) {
            return $model->requested_no;
        })
        ->addColumn('requestor', function ($model) {
            return $model->user->user->first_name . ' ' . $model->user->user->last_name;
        })
        ->addColumn('requested_form', function ($model) {
            return $model->form->name;
        })
        ->addColumn('status', function ($model) {
            return ucwords(str_replace('_', ' ', $model->status));
        })
        ->addColumn('priority', function ($model) {
            return $model->priority;
        })
        ->addColumn('action', function ($model) {
            return action_data(
                null,
                route('request-forms.delete', $model->id),
                $this->module
            );
        })->make(true);

        return $dt;
    }


    public function downloadDocs($docId)
    {
        $document = RequestorDocument::findOrFail($docId);
        if ($document) {
            return response()->download(storage_path('app/public/' . $document->file), $document->file_name);
        }
        // $file = Storage::disk('public')->get($document->file);
  
        // return (new Response($file, 200))
        //       ->header('Content-Type', 'txt');
    }
}
