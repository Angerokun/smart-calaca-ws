<?php

namespace App\Http\Controllers;

use App\Module;
use App\ModulePermission;
use App\Restriction;
use App\UserPermission;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

use function App\Common\modulePermission;
use function App\Datatable\action_data;

class AccessControlController extends Controller
{
    private $module;
    /**
     * construct
     */
    public function __construct()
    {
        $this->module = Module::where('module_name', 'access_control')->first();
    }

    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        return view(
            'access-control.index'
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modules = Module::where('access', '!=', null)->get();
        $restrictions = Restriction::all();

        return view(
            'access-control.create',
            [
                'modules' => $modules,
                'restrictions' => $restrictions
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userPermission = UserPermission::create($this->payload($request));

        foreach ($request->modules as $key => $module) {
            if ($request->input('access_' . $module) != null) {
                ModulePermission::create([
                    'permission_id' => $userPermission->id,
                    'module_id' => $module,
                    'access' => json_encode($request->input('access_' . $module))
                ]);
            }
        }

        return array(
            "status" => "success",
            'data' => $userPermission,
            'desc' => 'Access Control Successfully Created!'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $accessId)
    {
        $userPermission = UserPermission::findOrFail($accessId);

        return view(
            'access-control.show',
            ["accessControl" =>  $userPermission]
        );
    }

    /**
     * edit
     *
     * @param int $accessId
     *
     * @return void
     */
    public function edit(int $accessId)
    {
        $userPermission = UserPermission::findOrFail($accessId);
        $modules = Module::where('access', '!=', null)->get();
        $restrictions = Restriction::all();

        return view(
            'access-control.edit',
            [
                "accessControl" =>  $userPermission,
                'modules' => $modules,
                'restrictions' => $restrictions
            ]
        );
    }

    /**
     * update
     *
     * @param Request $request
     * @param int $accessId
     *
     * @return void
     */
    public function update(Request $request, int $accessId)
    {
        $userPermission = UserPermission::findOrFail($accessId);

        $userPermission->update($this->payload($request));

        $moduleId = array();
        $dataArr = array();
        foreach ($userPermission->modulePermission()->get() as $key => $ids) {
            $moduleId[] = $ids->module_id;
        }

        foreach ($request->modules as $key => $module) {
            if ($request->input('access_' . $module) != null) {
                if (in_array($module, $moduleId)) {
                    $dataArr[] = (int)$module;
                    $userPermission->modulePermission()->where('module_id', $module)->update([
                        'permission_id' => $userPermission->id,
                        'module_id' => $module,
                        'access' => json_encode($request->input('access_' . $module))
                    ]);
                } else {
                    ModulePermission::create([
                        'permission_id' => $userPermission->id,
                        'module_id' => $module,
                        'access' => json_encode($request->input('access_' . $module))
                    ]);
                }
            }
        }
        $result = array_diff($moduleId, $dataArr);

        foreach ($result as $id) {
            $permission = ModulePermission::where('module_id', $id)
                ->where('permission_id', $userPermission->id)->first();
                
            $permission->delete();
        }

        return array(
            "status" => "success",
            'data' => $userPermission,
            'desc' => 'Access Control Successfully Updated!'
        );
    }
    
    /**
     * destroy
     *
     * @param int $permissionId
     *
     * @return void
     */
    public function destroy(int $accessId)
    {
        $permission = UserPermission::findOrFail($accessId);

        $permission->delete();

        return redirect(route('access-controls.index'));
    }

    /**
     * delete
     *
     * @param int $accessId
     *
     * @return void
     */
    public function delete(int $accessId)
    {
        $accessControl = UserPermission::findOrFail($accessId);

        return view(
            'access-control.delete',
            ['accessControl' => $accessControl]
        );
    }

    /**
     * datatables
     *
     * @return void
     */
    public function accessControlDatatable()
    {
        $userPermissions = UserPermission::all();

        $dt = DataTables::of($userPermissions)
        ->addColumn('name', function ($model) {
            return $model->name;
        })
        ->addColumn('description', function ($model) {
            return $model->description;
        })
        ->addColumn('user_type', function ($model) {
            return ucwords($model->user_type);
        })
        ->addColumn('action', function ($model) {
            return action_data(
                route('access-controls.edit', $model->id),
                route('access-controls.delete', $model->id),
                $this->module
            );
        })->make(true);

        return $dt;
    }

    /**
     * Validation Rules for Store and Update
     *
     * @param Request $request
     *
     * @return array
     */
    public function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'name' => 'required',
                'description' => 'nullable',
                'user_type' => 'required'
            ]
        );
    }
}
