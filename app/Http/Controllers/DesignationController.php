<?php

namespace App\Http\Controllers;

use App\Department;
use App\Designation;
use App\Module;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

use function App\Common\modulePermission;
use function App\Datatable\action_data;

class DesignationController extends Controller
{
    private $module;
    /**
     * construct
     */
    public function __construct()
    {
        $this->module = Module::where('module_name', 'designation')->first();
    }

    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        return view(
            'designation.index'
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();

        return view(
            'designation.create',
            ['departments' => $departments]
        );
    }

    /**
     * store
     *
     * @param Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $designation = Designation::create($this->payload($request));

        return array(
            "status" => "success",
            'data' => $designation,
            'desc' => 'Designation ' . $designation->name . ' Successfully Created!'
        );
    }

    /**
     * show
     *
     * @param int $designationId
     *
     * @return void
     */
    public function show(int $designationId)
    {
        $designation = Designation::findOrFail($designationId);

        return view(
            'designation.show',
            ['designation' => $designation]
        );
    }

    /**
     * edit
     *
     * @param int $designationId
     *
     * @return void
     */
    public function edit(int $designationId)
    {
        $designation = Designation::findOrFail($designationId);

        $departments = Department::all();

        return view(
            'designation.edit',
            [
                'designation' => $designation,
                'departments' => $departments
            ]
        );
    }

    /**
     * update
     *
     * @param Request $request
     * @param int $designationId
     *
     * @return void
     */
    public function update(Request $request, int $designationId)
    {
        $designation = Designation::findOrFail($designationId);

        $designation->update($this->payload($request));

        return array(
            "status" => "success",
            'data' => $designation,
            'desc' => 'Designation Record Successfully Updated!'
        );
    }

    /**
     * delete
     *
     * @param int $designationId
     *
     * @return void
     */
    public function delete(int $designationId)
    {
        $designation = Designation::findOrFail($designationId);

        return view(
            'designation.delete',
            ['designation' => $designation]
        );
    }

    /**
     * destroy
     *
     * @param int $designationId
     *
     * @return void
     */
    public function destroy(int $designationId)
    {
        $designation = Designation::findOrFail($designationId);

        $designation->delete();

        return redirect(route('designations.index'));
    }

    /**
     * datatables
     *
     * @return void
     */
    public function designationtDatatable()
    {
        $designation = Designation::all();

        $dt = DataTables::of($designation)
        ->addColumn('id', function ($model) {
            return $model->id;
        })
        ->addColumn('name', function ($model) {
            return $model->name;
        })
        ->addColumn('department', function ($model) {
            return $model->department->name;
        })
        ->addColumn('description', function ($model) {
            return $model->description != null ? $model->description : 'N/A';
        })
        ->addColumn('action', function ($model) {
            return action_data(
                route('designations.edit', $model->id),
                route('designations.delete', $model->id),
                $this->module
            );
        })->make(true);

        return $dt;
    }

    /**
     * Validation Rules for Store and Update
     *
     * @param Request $request
     *
     * @return array
     */
    public function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'department_id' => 'required',
                'name' => 'required',
                'description' => 'nullable'
            ]
        );
    }
}
