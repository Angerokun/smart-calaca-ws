<?php

namespace App\Http\Controllers;

use App\Department;
use App\DigitalForm;
use App\Form;
use App\FormRequirement;
use App\Module;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

use function App\Common\modulePermission;
use function App\Datatable\action_data;

class FormController extends Controller
{
    private $module;
    /**
     * construct
     */
    public function __construct()
    {
        $this->module = Module::where('module_name', 'forms')->first();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('form.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();

        $digitalForms = DigitalForm::all();

        return view(
            'form.create',
            [
                'departments' => $departments,
                'digitalForms' => $digitalForms
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $form = Form::create($this->payload($request));

        foreach ($request->input('for_requirements') as $key => $value) {
            $request->merge([
                'form_id' => $form->id,
                'file_id' => $request->input('file_id' . $value),
                'name' => $request->input('name' . $value),
                'description' => $request->input('description' . $value),
            ]);
            $formReq = FormRequirement::create($this->requirementPayload($request));
        }

        return array(
            "status" => "success",
            'data' => $form,
            'desc' => 'Form ' . $form->name . ' Successfully Created!'
        );
    }

    /**
     * show
     *
     * @param int $formId
     *
     * @return void
     */
    public function show(int $formId)
    {
        $form = Form::findOrFail($formId);

        return view(
            'form.show',
            ['form' => $form]
        );
    }

   /**
    * edit
    *
    * @param int $formId
    *
    * @return void
    */
    public function edit(int $formId)
    {
        $form = Form::findOrFail($formId);

        $departments = Department::all();

        return view(
            'form.edit',
            [
                'form' => $form,
                'departments' => $departments
            ]
        );
    }

    /**
     * update
     *
     * @param Request $request
     * @param int $formId
     *
     * @return void
     */
    public function update(Request $request, int $formId)
    {
        $form = Form::findOrFail($formId);

        $form->update($this->payload($request));

        return array(
            "status" => "success",
            'data' => $form,
            'desc' => 'Form Record Successfully Updated!'
        );
    }

    /**
     * delete
     *
     * @param int $formId
     *
     * @return void
     */
    public function delete(int $formId)
    {
        $form = Form::findOrFail($formId);

        return view(
            'form.delete',
            ['form' => $form]
        );
    }

    /**
     * destroy
     *
     * @param int $formId
     *
     * @return void
     */
    public function destroy(int $formId)
    {
        $form = Form::findOrFail($formId);

        $form->delete();

        return redirect(route('forms.index'));
    }

    /**
     * datatables
     *
     * @return void
     */
    public function formDatatable()
    {
        $forms = Form::all();

        $dt = DataTables::of($forms)
        ->addColumn('id', function ($model) {
            return $model->id;
        })
        ->addColumn('name', function ($model) {
            return $model->name;
        })
        ->addColumn('department', function ($model) {
            return $model->department->name;
        })
        ->addColumn('description', function ($model) {
            return $model->description != null ? $model->description : 'N/A';
        })
        ->addColumn('action', function ($model) {
            return action_data(
                route('forms.edit', $model->id),
                route('forms.delete', $model->id),
                $this->module
            );
        })->make(true);

        return $dt;
    }

    /**
     * Validation Rules for Store and Update
     *
     * @param Request $request
     *
     * @return array
     */
    public function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'department_id' => 'required',
                'name' => 'required',
                'description' => 'nullable'
            ]
        );
    }

    /**
     * Validation Rules for Store and Update
     *
     * @param Request $request
     *
     * @return array
     */
    public function requirementPayload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'form_id' => 'required',
                'file_id' => 'nullable',
                'name' => 'required',
                'description' => 'nullable'
            ]
        );
    }
}
