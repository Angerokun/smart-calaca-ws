<?php

namespace App\Http\Controllers;

use App\EmergencyRequest;
use App\RequestForm;
use App\RequestorProfile;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $faker = factory(RequestForm::class, 100)->create();
        $request_forms = RequestForm::all();
        $requestors = RequestorProfile::all();
        $requests_by_month = RequestForm::select(
            DB::raw('count(id) as `data`'),
            DB::raw("DATE_FORMAT(created_at, '%m-%Y') new_date"),
            DB::raw('MONTH(created_at) month')
        )->whereYear('created_at', '2021')
            ->groupby('month')->get();
        $request_arr = [
            'month' => [
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December'
            ],
            'count' => [0,0,0,0,0,0,0,0,0,0,0,0]
        ];
        foreach ($requests_by_month as $month) {
            $request_arr['count'][($month->month - 1)] =  $month->data;
        }
        
        return view('home', [
            'requestors' => $requestors,
            'request_forms' => $request_forms,
            'emergency_requests' => EmergencyRequest::take(5)->get(),
            'requests_in_progress' => RequestForm::status(RequestForm::STATUS['IN_PROGRESS'])->get(),
            'requests_assigned' => RequestForm::status(RequestForm::STATUS['ASSIGNED'])->get(),
            'requests_pending' => RequestForm::status(RequestForm::STATUS['PENDING'])->get(),
            'requests_approved' => RequestForm::status(RequestForm::STATUS['APPROVED'])->get(),
            'requests_disapproved' => RequestForm::status(RequestForm::STATUS['DISAPPROVED'])->get(),
            'requests_request_status' => RequestForm::status(RequestForm::STATUS['REQUEST'])->get(),
            'request_per_month' => $request_arr
        ]);
    }
}
