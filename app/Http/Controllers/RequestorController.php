<?php

namespace App\Http\Controllers;

use App\Module;
use App\RequestorProfile;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

use function App\Common\modulePermission;
use function App\Datatable\action_data;

class RequestorController extends Controller
{
    private $module;
    /**
     * construct
     */
    public function __construct()
    {
        $this->module = Module::where('module_name', 'requestor')->first();
    }

    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        return view(
            'requestor.index'
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('requestor.show', [
            'requestor' => RequestorProfile::findOrFail($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('requestor.edit', [
            'requestor' => RequestorProfile::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {   
        $requestorProfile = RequestorProfile::findOrFail($id);
        $requestorProfile = $requestorProfile->update($this->payload($request));
        return array(
            "status" => "success",
            "data" => RequestorProfile::findOrFail($id),
            'desc' => 'Requestor Record Successfully Updated!'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        $requestorProfile = RequestorProfile::findOrFail($id);

        return view(
            'requestor.delete',
            ['requestor' => $requestorProfile]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $requestorProfile = RequestorProfile::findOrFail($id);

        $user = User::findOrFail($requestorProfile->user->id);

        $user->delete();

        $requestorProfile->delete();

        return redirect(route('requestor-profiles.index'));
    }

    /**
     * datatables
     *
     * @return void
     */
    public function requestorDatatable()
    {
        $requestor = RequestorProfile::all();

        $dt = DataTables::of($requestor)
        ->addColumn('id', function ($model) {
            return $model->id;
        })
        ->addColumn('name', function ($model) {
            return $model->last_name . ', ' . $model->first_name;
        })
        ->addColumn('email', function ($model) {
            return $model->user->email;
        })
        ->addColumn('contact_no', function ($model) {
            return $model->contact_no;
        })
        ->addColumn('birth_date', function ($model) {
            return date('F d, Y', strtotime($model->birth_date));
        })
        ->addColumn('address', function ($model) {
            return $model->address != null ? $model->address : 'N/A';
        })
        ->addColumn('action', function ($model) {
            return action_data(
                route('requestor-profiles.edit', $model->id),
                route('requestor-profiles.delete', $model->id),
                $this->module
            );
        })->make(true);

        return $dt;
    }

    public function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'first_name' => 'required',
                'middle_name' => 'nullable',
                'last_name' => 'required',
                'birth_date' => 'required|date',
                'contact_no' => 'nullable',
                'address' => 'nullable',
            ]
        );
    }
}
