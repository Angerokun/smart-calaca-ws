<?php

namespace App\Http\Controllers;

use App\AdminProfile;
use App\Designation;
use App\Module;
use App\User;
use App\UserAccess;
use App\UserPermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

use function App\Common\modulePermission;
use function App\Datatable\action_data;

class AdministratorController extends Controller
{
    private $module;
    /**
     * construct
     */
    public function __construct()
    {
        $this->module = Module::where('module_name', 'admin')->first();
    }

    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        return view(
            'administrator.index'
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = UserPermission::all();

        $designations = Designation::all();
        
        return view(
            'administrator.create',
            [
                'permissions' => $permissions,
                'designations' => $designations
            ]
        );
    }

    /**
     * store
     *
     * @param Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {

        $userPayload = $this->payloadAccount($request);

        $adminProfile = AdminProfile::create($this->payload($request));
        
        $user = User::create([
            'name' => $userPayload['name'],
            'email' => $userPayload['email'],
            'password' => Hash::make($userPayload['password']),
            'user_type' => 'App\AdminProfile',
            'user_id' => $adminProfile->id
        ]);

        $access = UserAccess::create([
            'user_id' => $user->id,
            'permission_id' => $request->input('permission_id')
        ]);
        
        return array(
            "status" => "success",
            'data' => $user,
            'desc' => 'Admin Account Successfully Created!'
        );
    }

    /**
     * show
     *
     * @param string $adminId
     *
     * @return void
     */
    public function show(string $adminId)
    {
        $adminProfile = AdminProfile::findOrFail($adminId);
        $user_access = UserAccess::where('user_id', $adminProfile->user->id)->first();
        return view(
            'administrator.show',
            [
                "adminProfile" =>  $adminProfile,
                "userAccess" => $user_access
            ]
        );
    }

    /**
     * edit
     *
     * @param int $adminId
     *
     * @return void
     */
    public function edit(int $adminId)
    {
        $adminProfile = AdminProfile::findOrFail($adminId);

        $permissions = UserPermission::all();

        $designations = Designation::all();

        return view(
            'administrator.edit',
            [
                'adminProfile' => $adminProfile,
                'permissions' => $permissions,
                'designations' => $designations
            ]
        );
    }

    /**
     * update
     *
     * @param Request $request
     * @param int $adminId
     *
     * @return void
     */
    public function update(
        Request $request,
        int $adminId
    ) {
        $adminProfile = AdminProfile::findOrFail($adminId);

        $adminProfile->update($this->payload($request));

        return array(
            "status" => "success",
            'data' => $adminProfile,
            'desc' => 'Admin Account Successfully Updated!'
        );
    }

    /**
     * delete
     *
     * @param int $adminId
     *
     * @return void
     */
    public function delete(string $adminId)
    {
        $adminProfile = AdminProfile::findOrFail($adminId);

        return view(
            'administrator.delete',
            ['adminProfile' => $adminProfile]
        );
    }

    /**
     * destroy
     *
     * @param int $adminId
     *
     * @return void
     */
    public function destroy(int $adminId)
    {
        $adminProfile = AdminProfile::findOrFail($adminId);

        $user = User::findOrFail($adminProfile->user->id);

        $user->delete();

        $adminProfile->delete();

        return redirect(route('admin-profiles.index'));
    }

    /**
     * Validation Rules for Store and Update
     *
     * @param Request $request
     *
     * @return array
     */
    public function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'first_name' => 'required',
                'middle_name' => 'nullable',
                'last_name' => 'required',
                'designation_id' => 'required',
                'birth_date' => 'required',
                'contact_no' => 'nullable'
            ]
        );
    }

    /**
     * Validation Rules for Store and Update for account
     *
     * @param Request $request
     *
     * @return array
     */
    public function payloadAccount(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'name' => [
                    'required',
                    'string',
                    'max:255'
                ],
                'email' => [
                    'required',
                    'string',
                    'email',
                    'max:255'
                ],
                'password' => [
                    'required',
                    'string',
                    'min:8',
                    'confirmed'
                ],
                'user_type' => 'nullable',
                'user_id' => 'nullable'
            ]
        );
    }

    /**
     * edit account
     *
     * @param int $adminId
     *
     * @return void
     */
    public function editAccount(int $adminId)
    {
        $permissions = UserPermission::all();
        $adminProfile = AdminProfile::findOrFail($adminId);
        return view(
            'administrator.edit-account',
            [
                'permissions' => $permissions,
                'adminProfile' => $adminProfile
            ]
        );
    }

    /**
     * change password
     *
     * @param Request $request
     *
     * @return void
     */
    public function updateAccount(
        Request $request,
        int $userId
    ) {
        $user = User::findOrFail($userId);

        $requestData = $this->payloadAccount($request);
        $requestData['password'] = Hash::make($request->password);

        $user->update($requestData);

        return array(
            "status" => "success",
            'data' => $user,
            'desc' => 'Account Successfully Updated!'
        );
    }
    /**
     * datatables
     *
     * @return void
     */
    public function adminProfileDatatable()
    {
        $department = AdminProfile::all();

        $dt = DataTables::of($department)
        ->addColumn('id', function ($model) {
            return $model->id;
        })
        ->addColumn('name', function ($model) {
            return $model->last_name . ', ' . $model->first_name;
        })
        ->addColumn('position', function ($model) {
            return $model->designation->name;
        })
        ->addColumn('department', function ($model) {
            return $model->designation->department->name;
        })
        ->addColumn('contact_no', function ($model) {
            return $model->contact_no;
        })
        ->addColumn('birth_date', function ($model) {
            return date('F d, Y', strtotime($model->birth_date));
        })
        ->addColumn('action', function ($model) {
            return action_data(
                route('admin-profiles.edit', $model->id),
                route('admin-profiles.delete', $model->id),
                $this->module
            );
        })->make(true);

        return $dt;
    }
}
