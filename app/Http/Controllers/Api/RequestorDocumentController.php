<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\RequestorDocument as RequestorDocumentResource;
use App\RequestForm;
use App\RequestorDocument;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class RequestorDocumentController extends Controller
{
    /**
     * index
     *
     * @param int $requestorId
     * @param int $userId
     *
     * @return void
     */
    public function index(int $userId, int $requestedId)
    {
        $documents = RequestForm::findOrFail($requestedId)
            ->documents()->where('user_id', $userId)->get();

        return RequestorDocumentResource::collection($documents);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(
        Request $request,
        int $userId,
        int $requestedId
    ): JsonResource {
        $file = $request->file('file');
        $file_name = $request->file('file')->getClientOriginalName();
        $fileUrl = now()->timestamp . "_" . $file_name . '.' . $file->getClientOriginalExtension();

        $document = RequestorDocument::create([
            'file' => $fileUrl,
            'file_name' => $file_name,
            'requested_id' => $requestedId,
            'user_id' => $userId
        ]);

        if ($document) {
            //store to storage path
            $file_for_upload = File::get($file);
            $upload = Storage::disk('public')->put($fileUrl, $file_for_upload);
        }

        return new RequestorDocumentResource($document);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Validation Rules for Store and Update
     *
     * @param Request $request
     * @return array
     */
    private function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'requested_id' => 'required',
                'user_id' => 'required',
                'remarks' => 'nullable',
                'file_name' => 'required',
                'file' => 'required'
            ]
        );
    }
}
