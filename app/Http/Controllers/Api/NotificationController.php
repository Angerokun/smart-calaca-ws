<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Notification;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{

    /**
     * get all notification by user
     *
     * @return ResourceCollection
     */
    public function getAllUserNotification(): ResourceCollection
    {
        $notification = Auth::User()->notifications->take(10);

        return Notification::collection($notification);
    }

    /**
     * Get unread Notification
     *
     * @return void
     */
    public function getUnreadNotificationCount()
    {
        return Auth::User()->unreadNotifications->count();
    }
    
    /**
     * Get read all Notifications
     *
     * @return void
     */
    public function readAllNotification()
    {
        return Auth::User()->unreadNotifications->markAsRead();
    }
    

    /**
     * mark view notification
     *
     * @param string $id
     *
     * @return void
     */
    public function markAsViewNotification(string $id)
    {
        $notification = Auth::User()->notifications->where('id', $id)->first();
        return $notification->markAsView();
    }
}
