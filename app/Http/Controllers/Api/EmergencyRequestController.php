<?php

namespace App\Http\Controllers\Api;

use App\DepartmentHotline;
use App\EmergencyRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\DepartmentHotline as DepartmentHotlineResource;
use App\Http\Resources\EmergencyRequest as EmergencyRequestResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

use function App\Log\log_created;

class EmergencyRequestController extends Controller
{
    /**
     * index
     *
     * @param int $userId
     *
     * @return ResourceCollection
     */
    public function index(int $userId): ResourceCollection
    {
        $emergency = EmergencyRequest::where('user_id', $userId)->get();

        return EmergencyRequestResource::collection($emergency);
    }

    /**
     * store
     *
     * @param Request $request
     * @param int $userId
     *
     * @return JsonResource
     */
    public function store(
        Request $request,
        int $userId
    ): JsonResource {
        $request->merge(['user_id' => $userId]);

        $emergency = EmergencyRequest::create($this->payload($request));

        log_created($emergency);

        return new EmergencyRequestResource($emergency);
    }

    /**
     * show
     *
     * @param int $userId
     * @param int $requestId
     *
     * @return JsonResource
     */
    public function show(int $userId, int $requestId): JsonResource
    {
        $emergency = EmergencyRequest::where('user_id', $userId)
            ->where('id', $requestId)->first();

        return new EmergencyRequestResource($emergency);
    }

    /**
     * hotlines
     *
     * @return ResourceCollection
     */
    public function hotlines(): ResourceCollection
    {
        $hotlines = DepartmentHotline::all();

        return DepartmentHotlineResource::collection($hotlines);
    }

    /**
     * hotlines
     *
     * @return ResourceCollection
     */
    public function showHotline(int $hotlineId): JsonResource
    {
        $hotline = DepartmentHotline::findOrFail($hotlineId);

        return new DepartmentHotlineResource($hotline);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * payload for Update and Create Emergency Request
     *
     * @param Request $request
     *
     * @return array
     */
    public function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'user_id' => 'required',
                'department_id' => 'required',
                'area_incident' => 'required',
                'type_incident' => 'required',
                'remarks' => 'required'
            ]
        );
    }
}
