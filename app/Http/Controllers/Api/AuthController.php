<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\User as UserResource;
use App\RequestorProfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Register
     *
     * @param Request $request
     *
     * @return void
     */
    public function register(Request $request)
    {
        $getuser = RequestorProfile::where('contact_no', $request->contact_no)->first();
        if ($getuser) {
            $requestorId = $getuser->id;
        } else {
            $requestorProfile = RequestorProfile::create($this->payloadRequestor($request));
            $requestorId = $requestorProfile->id;
        }
        

        $userAccount = $this->payloadAccount($request);

        $user = User::create([
            'name' => $userAccount['name'],
            'email' => $userAccount['email'],
            'password' => Hash::make($userAccount['password']),
            'user_type' => 'App\RequestorProfile',
            'user_id' => $requestorId
        ]);


        $accessToken = $user->createToken('authToken')->accessToken;

        return response()->json(
            [
                'user' => new UserResource($user),
                'token' => $accessToken,
                'token_type' => 'Bearer'
            ],
            201
        );
    }

    /**
     * logout
     *
     * @param Request $request
     *
     * @return void
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * login
     *
     * @param Request $request
     *
     * @return void
     */
    public function login(Request $request)
    {
        if (!auth()->attempt($this->loginPayload($request))) {
            return response()->json(['message' => 'Invalid Credentials'], 401);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response(
            [
                'user' => new UserResource(auth()->user()),
                'token' => $accessToken,
                'token_type' => 'Bearer'
            ]
        );
    }

    /**
     * payload for login
     *
     * @param Request $request
     *
     * @return array
     */
    public function loginPayload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'email' => 'required|email',
                'password' => 'required'
            ]
        );
    }

    /**
     * payload for register
     *
     * @param Request $request
     *
     * @return array
     */
    public function payloadRequestor(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'first_name' => 'required',
                'middle_name' => 'nullable',
                'last_name' => 'required',
                'birth_date' => 'required',
                'contact_no' => 'required|unique:requestor_profiles,contact_no',
                'address' => 'nullable'
            ]
        );
    }

    /**
     * payload for register
     *
     * @param Request $request
     *
     * @return array
     */
    public function payloadAccount(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|confirmed'
            ]
        );
    }

    /**
     * user Information
     *
     * @param Request $request
     *
     * @return void
     */
    public function userProfile(Request $request): JsonResource
    {
        $user = $request->user();

        return new UserResource($user);
    }
}
