<?php

namespace App\Http\Controllers\Api;

use App\Helpers\RequestCodeGenerator;
use App\Http\Controllers\Controller;
use App\Http\Resources\RequestForm as RequestFormResource;
use App\RequestForm;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

use function App\Log\log_created;

class RequestFormController extends Controller
{
    /**
     * get Request by user
     *
     * @param int $userId
     *
     * @return ResourceCollection
     */
    public function index(int $userId): ResourceCollection
    {
        $requestUser = RequestForm::where('user_id', $userId)->get();

        return RequestFormResource::collection($requestUser);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * store
     *
     * @param Request $request
     *
     * @return JsonResource
     */
    public function store(Request $request, int $userId): JsonResource
    {
        $generateCodeNo = new RequestCodeGenerator();

        $request->merge(
            [
                'requested_no' => $generateCodeNo->generateCodeNumber(),
                'user_id' => $userId,
                'status' => RequestForm::STATUS['REQUEST'],
                'priority' => RequestForm::PRIORITY['LOW']
            ]
        );

        $requestForm = RequestForm::create($this->payload($request));

        log_created($requestForm);

        return new RequestFormResource($requestForm);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $userId, int $reqId): JsonResource
    {
        $requestedForm = RequestForm::where('user_id', $userId)
            ->where('id', $reqId)->first();

        return new RequestFormResource($requestedForm);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * payload for Update and Create Request Form
     *
     * @param Request $request
     *
     * @return array
     */
    public function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'requested_no' => 'required',
                'user_id' => 'nullable',
                'form_id' => 'required',
                'status' => 'nullable',
                'description' => 'nullable',
                'priority' => 'nullable'
            ]
        );
    }
}
