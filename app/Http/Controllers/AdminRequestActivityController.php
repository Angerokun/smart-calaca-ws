<?php

namespace App\Http\Controllers;

use App\AdminProfile;
use App\AdminRequestActivity;
use App\Designation;
use App\RequestForm;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

use function App\Log\log_created;

class AdminRequestActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(int $requestedId)
    {
        

        $requestForm = RequestForm::findOrFail($requestedId);

        $department = $requestForm->form->department_id;

        $designation = Designation::where('department_id', $department)->get();

        $designationId = array();
        foreach ($designation as $key => $value) {
            $designationId[] = $value->id;
        }
        
        $users = AdminProfile::whereIn('designation_id', $designationId)->get();

        return view('admin-activity.create', [
            'users' => $users,
            'requestedId' => $requestedId,
            'request_status' => $requestForm->status,
            'priority' => $requestForm->priority
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, int $requestedId)
    {
        $request->merge(['requested_id' => $requestedId]);


        $activity = AdminRequestActivity::create($this->payload($request));

        if ($request->has('file')) {
            $file = $request->file('file');
            $file_name = $request->file('file')->getClientOriginalName();
            $fileUrl = now()->timestamp . "_" . $file_name . '.' . $file->getClientOriginalExtension();

            $activity->update([
                'file' => $fileUrl,
                'file_name' => $file_name,
            ]);

            //store to storage path
            $file_for_upload = File::get($file);
            $upload = Storage::disk('public')->put($fileUrl, $file_for_upload);
        }

        if ($request->input('status') != null) {
            $requestForm = RequestForm::findOrFail($requestedId);

            $requestForm->update([
                'status' => $request->input('status'),
                'priority' => $request->input('priority')
            ]);
        }

        log_created($activity);

        // return array(
        //     "status" => "success",
        //     'redirect' => true,
        //     'route' => route('request-forms.show', $activity->requested_id),
        //     'data' => $activity,
        //     'desc' => 'Acitivty Successfully Created!'
        // );
        return redirect(route('request-forms.show', $activity->requested_id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $requestedId, int $adminReqId)
    {
        $activity = AdminRequestActivity::findOrFail($adminReqId);

        return view('admin-activity.show', [
            'activity' => $activity
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Validation Rules for Store and Update
     *
     * @param Request $request
     *
     * @return array
     */
    public function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'requested_id' => 'required',
                'assigned_to' => 'nullable',
                'status' => 'nullable',
                'remarks' => 'required',
                'subject' => 'required',
                'file_name' => 'nullable',
                'file' => 'nullable',
                'priority' => 'required',
                'update_user' => 'nullable',
                'update_info' => 'nullable'
            ]
        );
    }

    public function downloadDocs($docId)
    {
        $document = AdminRequestActivity::findOrFail($docId);
        
        if ($document) {
            return response()->download(storage_path('app/public/' . $document->file), $document->file_name);
        }
    }
}
