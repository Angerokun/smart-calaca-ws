<?php

namespace App\Http\Controllers;

use App\EmergencyRequest;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Module;

use function App\Common\modulePermission;
use function App\Datatable\action_data;

class EmergencyRequestController extends Controller
{
    private $module;
    /**
     * construct
     */
    public function __construct()
    {
        $this->module = Module::where('module_name', 'emergency_request')->first();
    }

    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        return view(
            'emergency-request.index'
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $requestId)
    {
        $emergency = EmergencyRequest::findOrFail($requestId);

        return view(
            'emergency-request.show',
            ['emergency' => $emergency]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * delete
     *
     * @param int $requestId
     *
     * @return void
     */
    public function delete(int $requestId)
    {
        $emergency = EmergencyRequest::findOrFail($requestId);

        return view(
            'emergency-request.delete',
            ['emergency' => $emergency]
        );
    }

    /**
     * destroy
     *
     * @param int $requestId
     *
     * @return void
     */
    public function destroy(int $requestId)
    {
        $emergency = EmergencyRequest::findOrFail($requestId);

        $emergency->delete();

        return redirect(route('emergency-requests.index'));
    }

    /**
     * datatables
     *
     * @return void
     */
    public function emergencyRequestDatatable()
    {
        $request = EmergencyRequest::all();

        $dt = DataTables::of($request)
        ->addColumn('id', function ($model) {
            return $model->id;
        })
        ->addColumn('date', function ($model) {
            return $model->created_at;
        })
        ->addColumn('department', function ($model) {
            return $model->department->name;
        })
        ->addColumn('name', function ($model) {
            return $model->user->name;
        })
        ->addColumn('address', function ($model) {
            return $model->user->user->address ? $model->user->user->address : 'N/A';
        })
        ->addColumn('contact_number', function ($model) {
            return $model->user->user->contact_no;
        })
        ->addColumn('area_incident', function ($model) {
            return $model->area_incident;
        })
        ->addColumn('action', function ($model) {
            return action_data(
                null,
                route('emergency-requests.delete', $model->id),
                $this->module
            );
        })->make(true);

        return $dt;
    }
}
