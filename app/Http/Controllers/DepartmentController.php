<?php

namespace App\Http\Controllers;

use App\Department;
use App\DepartmentHotline;
use App\Module;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

use function App\Common\modulePermission;
use function App\Datatable\action_data;

class DepartmentController extends Controller
{
    private $module;
    /**
     * construct
     */
    public function __construct()
    {
        $this->module = Module::where('module_name', 'department')->first();
    }

    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        return view(
            'department.index'
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(
            'department.create'
        );
    }

    /**
     * store
     *
     * @param Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $department = Department::create($this->payload($request));

        if ($request->has('emergency')) {
            $request->merge(['department_id' => $department->id]);
            $hotlines = DepartmentHotline::create($this->payloadHotline($request));
        }

        return array(
            "status" => "success",
            'data' => $department,
            'desc' => 'Department' . $department->name . ' Successfully Created!'
        );
    }

    /**
     * show
     *
     * @param int $deptId
     *
     * @return void
     */
    public function show(int $deptId)
    {
        $department = Department::findOrFail($deptId);

        return view(
            'department.show',
            ['department' => $department]
        );
    }

    /**
     * edit
     *
     * @param int $deptId
     *
     * @return void
     */
    public function edit(int $deptId)
    {
        $department = Department::findOrFail($deptId);

        return view(
            'department.edit',
            ['department' => $department]
        );
    }

    /**
     * update
     *
     * @param Request $request
     * @param int $deptId
     *
     * @return void
     */
    public function update(Request $request, int $deptId)
    {
        $department = Department::findOrFail($deptId);

        $department->update($this->payload($request));

        if ($request->has('emergency')) {
            $request->merge(['department_id' => $department->id]);
            $department->hotline()->update($this->payloadHotline($request));
        }

        return array(
            "status" => "success",
            'data' => $department,
            'desc' => 'Department Record Successfully Updated!'
        );
    }

    /**
     * delete
     *
     * @param int $deptId
     *
     * @return void
     */
    public function delete(int $deptId)
    {
        $department = Department::findOrFail($deptId);

        return view(
            'department.delete',
            ['department' => $department]
        );
    }

    /**
     * destroy
     *
     * @param int $deptId
     *
     * @return void
     */
    public function destroy(int $deptId)
    {
        $department = Department::findOrFail($deptId);

        $department->delete();

        return redirect(route('departments.index'));
    }

    /**
     * datatables
     *
     * @return void
     */
    public function departmentDatatable()
    {
        $department = Department::all();

        $dt = DataTables::of($department)
        ->addColumn('id', function ($model) {
            return $model->id;
        })
        ->addColumn('name', function ($model) {
            return $model->name;
        })
        ->addColumn('description', function ($model) {
            return $model->description != null ? $model->description : 'N/A';
        })
        ->addColumn('action', function ($model) {
            return action_data(
                route('departments.edit', $model->id),
                route('departments.delete', $model->id),
                $this->module
            );
        })->make(true);

        return $dt;
    }

    /**
     * Validation Rules for Store and Update
     *
     * @param Request $request
     *
     * @return array
     */
    public function payload(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'name' => 'required',
                'description' => 'nullable'
            ]
        );
    }

    /**
     * Validation Rules for Store and Update
     *
     * @param Request $request
     *
     * @return array
     */
    public function payloadHotline(Request $request): array
    {
        return $this->validate(
            $request,
            [
                'department_id' => 'required',
                'mobile_number' => 'nullable',
                'telephone' => 'required'
            ]
        );
    }
}
