<?php
namespace App\Log;

use App\AdminProfile;
use App\Employee;
use App\Log;
use App\User;
use Illuminate\Support\Facades\Auth;

//function setup for create logs
if (!function_exists('log_created')) {
    function log_created($param, $related = null) {
        $log = new Log();
        $log->reference_id = $param->id;
        $log->reference_type = 'App\\' . class_basename($param);
        ($related != null ? $log->reference_relate_id = $related->id : '');
        ($related != null ? $log->reference_relate_type = 'App\\' . class_basename($related) : '');
        $log->user_id = Auth::user()->id;
        $log->description = class_basename($param) . ' created';
        $log->save();

        notifyUser($param);
    }

    function notifyUser($model)
    {
        $ids = getIdNotifyUser($model); // getting of users need to notify for this action
        if (count($ids) > 0) {
            $users = User::whereIn('id', $ids)->get();
            $notif = 'App\\Notifications\\' . class_basename($model);
            //Insert to Database Notification
            if (\Notification::send($users, new $notif($model))) {
                return back();
            }
        }
    }

    function getIdNotifyUser($model)
    {
        $userId = array('1');
        if (class_basename($model) == 'RequestForm') {
            $notif_id = array();

            foreach ($model->form->department->designations as $key => $value) {
                $notif_id[] = $value->id;
            }
            $admin_users = AdminProfile::whereIn('designation_id', $notif_id)->get();
            if ($admin_users) {
                foreach ($admin_users as $key => $value) {
                    array_push($userId, $value->user->id);
                }
            }
        }

        if (class_basename($model) == 'AdminRequestActivity') {
            $notif_id = array();

            foreach ($model->requestedForm->form->department->designations as $key => $value) {
                $notif_id[] = $value->id;
            }
            
            array_push($userId, $model->requestedForm->user_id);

            $admin_users = AdminProfile::whereIn('designation_id', $notif_id)->get();
            if ($admin_users) {
                foreach ($admin_users as $key => $value) {
                    array_push($userId, $value->user->id);
                }
            }
        }

        if (class_basename($model) == 'EmergencyRequest') {
            $notif_id = array();

            foreach ($model->department->designations as $key => $value) {
                $notif_id[] = $value->id;
            }
            $admin_users = AdminProfile::whereIn('designation_id', $notif_id)->get();
            if ($admin_users) {
                foreach ($admin_users as $key => $value) {
                    array_push($userId, $value->user->id);
                }
            }
        }
        return $userId;
    }
}
