<?php

namespace App\Common;

use App\ModulePermission;
use App\Restriction;
use App\UserAccess;
use DateTime;
use Illuminate\Support\Facades\Auth;

if (!function_exists('restrictions')) {
    function restrictions($access)
    {
        $array = array();
        $array = json_decode($access);
        $access_str = '';
        foreach ($array as $key => $value) {
            $restrict = Restriction::findOrFail($value);
            $access_str .= ucwords($restrict->name) . ', ';
        }
        return rtrim($access_str, ", ");
    }
}

if (!function_exists('modulePermission')) {
    function modulePermission($module)
    {
        if (Auth::User()->id == 1) {
            $restrictions = [
                'show_res' => 1,
                'create_res' => 1,
                'update_res' => 1,
                'delete_res' => 1,
                'print_res' => 1
            ];
        } else {
            $restrictions = [
                'show_res' => 0,
                'create_res' => 0,
                'update_res' => 0,
                'delete_res' => 0,
                'print_res' => 0
            ];
            $user_access = UserAccess::where('user_id', Auth::User()->id)->first();
            $module_permission = ModulePermission::where('permission_id', $user_access->permission_id)
                ->where('module_id', $module->id)->first();
            if ($module_permission) {
                $arr_ = array();
                $arr_ = json_decode($module_permission->access);
                foreach ($arr_ as $key => $restriction) {
                    switch ($restriction) {
                        case 1:
                            $restrictions['show_res'] = 1;
                            break;
                        case 2:
                            $restrictions['create_res'] = 1;
                            break;
                        case 3:
                            $restrictions['update_res'] = 1;
                            break;
                        case 4:
                            $restrictions['delete_res'] = 1;
                            break;
                        case 5:
                            $restrictions['print_res'] = 1;
                            break;
                    }
                }
            }
        }

        return $restrictions;
    }
}
