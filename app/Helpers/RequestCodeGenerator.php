<?php

namespace App\Helpers;

class RequestCodeGenerator
{
    /**
     * @return string
     */
    public function generateCodeNumber()
    {
        $len = 10;

        $hex = md5(time() . uniqid("", true));

        $pack = pack('H*', $hex);

        $tmp =  base64_encode($pack);

        $uid = preg_replace("#(*UTF8)[^A-Za-z0-9]#", "", $tmp);

        $len = max(4, min(128, $len));

        while (strlen($uid) < $len) {
            $uid .= self::getNonce(22);
        }

        $code = date('mdY') . strtoupper(substr($uid, 0, $len));

        return $code;
    }
}
