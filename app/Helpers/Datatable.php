<?php

namespace App\Datatable;

use App\Models\ModulePermission;
use App\Models\UserAccess;
use Illuminate\Support\Facades\Auth;

use function App\Common\modulePermission;

if (!function_exists('action_data')) {
    function action_data($edit, $delete, $module, $added = null)
    {
        $action = '';
        if (Auth::User()->id == 1) {
            if ($edit != null) {
                $action .= '<a href="' . $edit . '" class="btn btn-warning btn-circle btn-sm">
                                <i class="fas fa-edit"></i>
                            </a>';
            }
             
            if ($delete != null) {
                $action .= '<a data-url="' . $delete . '" title="Delete ' . $module->name .
                    ' Record" class="btn btn-danger btn-circle btn-sm popup_form" data-toggle="modal">
                        <i class="fas fa-trash"></i>
                    </a>';
            }
            return $action;
        } else {
            $restriction = modulePermission($module);
            if (
                $restriction['show_res'] == 0
                && $restriction['create_res'] == 0
                && $restriction['update_res'] == 0
                && $restriction['delete_res'] == 0
            ) {
                $action = '';
            } else {
                $action .= ($restriction['update_res'] == 1 ? '<a href="' . $edit .
                                '" class="btn btn-warning btn-circle btn-sm">
                                <i class="fas fa-edit"></i>
                            </a>' : '') .
                            ($restriction['delete_res'] == 1 ? '<a data-url="' . $delete .
                                '" title="Delete ' . $module->name .
                                ' Record" class="btn btn-danger btn-circle btn-sm popup_form" data-toggle="modal">
                                <i class="fas fa-trash"></i>
                            </a>' : '');
            }
            return $action;
        }
    }
}
