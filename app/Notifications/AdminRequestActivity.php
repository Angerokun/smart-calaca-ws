<?php

namespace App\Notifications;

use App\AdminRequestActivity as AdminRequestActivityRecord;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AdminRequestActivity extends Notification
{
    use Queueable;

    protected $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'notif_type' => 'admin_activity',
            'id'         => $this->request->id,
            'request_id' => $this->request->requested_id,
            'update_user'  => $this->request->update_user,
            'assigned_to' => $this->request->assigned_to,
            'requestor_info' => $this->customMessageRequestor($this->request),
            'other_info' => $this->customMessageAdmin($this->request),
            'route' => route('request-forms.show', $this->request->requested_id),
            'requestor_route' => '',
            'created_at' => $this->getCreatedAt($this->request->id)
        ];
    }

    public function getCreatedAt(string $id)
    {
        $log = AdminRequestActivityRecord::findOrFail($id)
            ->getLogs()->orderBy('created_at', 'DESC')->first();

        return $log->user->name;
    }

    public function customMessageRequestor($request)
    {
        $log = AdminRequestActivityRecord::findOrFail($request->id)
            ->getLogs()->orderBy('created_at', 'DESC')->first();

        if ($log->user_id == 1) {
            $user = 'Administrator';
        } else {
            $user = $log->user->user->designation->name;
        }
        
        $message = '';
        if ($request->assigned_to != null) {
            $message .= $user . ' assigned your request to ' .
                $request->user->user->designation->name . ' - ' . $request->subject . '.';
        }

        if ($request->status != null) {
            $message .= $user . ' updated status to ' .
                ucwords(str_replace('_', ' ', $request->status)) . ' - ' . $request->subject . '.';
        }

        return $message;
    }

    public function customMessageAdmin($request)
    {
        $log = AdminRequestActivityRecord::findOrFail($request->id)
            ->getLogs()->orderBy('created_at', 'DESC')->first();

        if ($log->user_id == 1) {
            $user = 'Administrator';
        } else {
            $user = $log->user->name;
        }

        $message = '';
        
        if ($request->assigned_to != null) {
            $message .= ' assigned a Request to ' . $request->user->name . ' - ' . $request->subject . '.';
        } elseif ($request->status != null) {
            $message .= ' updated status to ' .
                ucwords(str_replace('_', ' ', $request->status)) . ' - ' . $request->subject . '.';
        } else {
            $message .= ' add an update!' .
                ' - ' . $request->subject . '.';
        }

        return $message;
    }
}
