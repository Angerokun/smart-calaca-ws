<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminRequestActivity extends Model
{
    use SoftDeletes;

    protected $table = 'admin_request_activities';

    protected $fillable = [
        'requested_id',
        'assigned_to',
        'status',
        'remarks',
        'subject',
        'file_name',
        'file',
        'update_user',
        'update_info'
    ];


    /**
     * requested form relationship
     *
     * @return BelongsTo
     */
    public function requestedForm(): BelongsTo
    {
        return $this->belongsTo(RequestForm::class, 'requested_id');
    }

    /**
     * user relationship
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'assigned_to')->withTrashed();
    }

    /**
     * order by activities
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeOrderByActivities(Builder $query): Builder
    {
        return $query->orderBy('created_at', 'DESC');
    }

    /**
     * morphmany logs
     *
     * @return void
     */
    public function getLogs(): MorphMany
    {
        return $this->morphMany(Log::class, "reference");
    }
}
