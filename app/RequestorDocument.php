<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequestorDocument extends Model
{
    use SoftDeletes;

    protected $table = 'requestor_documents';

    protected $fillable = [
        'requested_id',
        'user_id',
        'remarks',
        'file_name',
        'file'
    ];

    /**
     * requested form relationship
     *
     * @return BelongsTo
     */
    public function requestedForm(): BelongsTo
    {
        return $this->belongsTo(RequestedForm::class, 'requested_id');
    }

    /**
     * user relationship
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id')->withTrashed();
    }
}
