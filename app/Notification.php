<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Notification extends Model
{
    protected $table = "notifications";

    /**
     * morph notification
     *
     * @return void
     */
    public function notifiable(): MorphTo
    {
        return $this->morphTo();
    }
}
