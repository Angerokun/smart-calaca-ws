<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Log extends Model
{
    protected $table = "logs";

    protected $primaryKey = 'id';

    protected $fillable = [
        'reference_id',
        'reference_type',
        'reference_relate_id',
        'reference_relate_type',
        'user_id',
        'description'
    ];
    
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $with = ['user'];

    /**
     * referense morph
     *
     * @return void
     */
    public function reference(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * user relationship
     *
     * @return void
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, "user_id", "id")->withTrashed();
    }
}
