<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAccess extends Model
{
    use SoftDeletes;

    protected $table = 'user_accesses';

    protected $fillable = [
        'user_id',
        'permission_id'
    ];

    /**
     * user relationship
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id')->withTrashed();
    }

    /**
     * user permission relationship
     *
     * @return BelongsTo
     */
    public function userPermission(): BelongsTo
    {
        return $this->belongsTo(UserPermission::class, 'permission_id');
    }
}
