<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class DepartmentHotline extends Model
{
    use SoftDeletes;

    protected $table = "department_hotlines";

    protected $fillable = [
        'department_id',
        'mobile_number',
        'telephone'
    ];

    /**
     * department relationship
     *
     * @return BelongsTo
     */
    public function department(): BelongsTo
    {
        return $this->belongsTo(Department::class, 'department_id');
    }
}
