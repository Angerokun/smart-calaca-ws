<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequestForm extends Model
{
    use SoftDeletes;

    protected $table = "requested_forms";

    protected $fillable = [
        'requested_no',
        'user_id',
        'form_id',
        'status',
        'description',
        'priority'
    ];

    const STATUS = [
        'REQUEST' => 'request',
        'ASSIGNED' => 'assigned',
        'IN_PROGRESS' => 'in_progress',
        'PENDING' => 'pending',
        'APPROVED' => 'approved',
        'DISAPPROVED' => 'disapproved'
    ];

    const PRIORITY = [
        'LOW' => 'low',
        'MEDIUM' => 'medium',
        'HIGH' => 'high'
    ];

    /**
     * user relationship
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id')->withTrashed();
    }

    /**
     * form relationship
     *
     * @return BelongsTo
     */
    public function form(): BelongsTo
    {
        return $this->belongsto(Form::class, 'form_id');
    }

    /**
     * documents relationship
     *
     * @return HasMany
     */
    public function documents(): HasMany
    {
        return $this->hasMany(RequestorDocument::class, 'requested_id');
    }

    /**
     * activities relationship
     *
     * @return HasMany
     */
    public function activities(): HasMany
    {
        return $this->hasMany(AdminRequestActivity::class, 'requested_id');
    }

    /**
     * morphmany logs
     *
     * @return void
     */
    public function getLogs(): MorphMany
    {
        return $this->morphMany(Log::class, "reference");
    }

    /**
     * status scope
     *
     * @return void
     */
    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }
}
