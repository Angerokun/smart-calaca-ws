<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Form extends Model
{
    use SoftDeletes;

    protected $table = 'forms';

    protected $fillable = [
        'department_id',
        'name',
        'description'
    ];

    /**
     * department relationship
     *
     * @return BelongsTo
     */
    public function department(): BelongsTo
    {
        return $this->belongsTo(Department::class, 'department_id');
    }

    /**
     * form requirements relationship
     *
     * @return HasMany
     */
    public function formRequirements(): HasMany
    {
        return $this->hasMany(FormRequirement::class, 'form_id');
    }
}
