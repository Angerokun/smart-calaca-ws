<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model
{
    use SoftDeletes;

    protected $table = 'modules';

    protected $fillable = [
        'module_id',
        'name',
        'module_name',
        'access',
        'icon',
        'routes'
    ];

    /**
     * module relationship
     *
     * @return BelongsTo
     */
    public function modules(): BelongsTo
    {
        return $this->belongsTo(Module::class, 'module_id');
    }
}
