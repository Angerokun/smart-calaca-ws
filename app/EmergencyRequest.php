<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmergencyRequest extends Model
{
    use SoftDeletes;

    protected $table = 'emergency_requests';

    protected $fillable = [
        'user_id',
        'department_id',
        'area_incident',
        'type_incident',
        'remarks'
    ];

    /**
     * user relationship
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id')->withTrashed();
    }

    /**
     * department relationship
     *
     * @return BelongsTo
     */
    public function department(): BelongsTo
    {
        return $this->belongsTo(Department::class, 'department_id');
    }
    
    /**
     * morphmany logs
     *
     * @return void
     */
    public function getLogs(): MorphMany
    {
        return $this->morphMany(Log::class, "reference");
    }
}
