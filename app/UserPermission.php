<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPermission extends Model
{
    use SoftDeletes;

    protected $table = 'user_permissions';

    protected $fillable = [
        'name',
        'description',
        'user_type'
    ];

    /**
     * module permission relationship
     *
     * @return HasMany
     */
    public function modulePermission(): HasMany
    {
        return $this->hasMany(ModulePermission::class, 'permission_id');
    }
}
