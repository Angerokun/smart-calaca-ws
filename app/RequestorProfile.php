<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequestorProfile extends Model
{
    use SoftDeletes;

    /**
     * table name
     *
     * @var string
     */
    protected $table = 'requestor_profiles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'birth_date',
        'contact_no',
        'address',
        'active'
    ];

    /**
     * morph one to user model
     *
     * @return MorphOne
     */
    public function user(): MorphOne
    {
        return $this->morphOne(User::class, "user")->withTrashed();
    }
}
