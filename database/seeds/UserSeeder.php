<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id' => 1,
            'name' => 'System Administrator',
            'email' => 'super_admin@gmail.com',
            'password' => '$2y$10$gmhPuUsW7ChwiVyJR/uDTuIgQ5CMWnUj2Veb1UQQYkpY6PsdBjkAy'
        ]);
    }
}
