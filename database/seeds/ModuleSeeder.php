<?php

use App\Module;
use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $home = Module::create([
            'module_id' => null,
            'name' => 'Home',
            'module_name' => 'home',
            'access' => null,
            'route' => null,
            'icon' => null
        ]);

        Module::create([
            'module_id' => $home->id,
            'name' => 'Dashboard',
            'module_name' => 'dashboard',
            'access' => '[1]',
            'route' => 'home',
            'icon' => 'fas fa-fw fa-tachometer-alt'
        ]);

        $user = Module::create([
            'module_id' => null,
            'name' => 'User Management',
            'module_name' => 'user',
            'access' => null,
            'route' => null,
            'icon' => null
        ]);

        Module::create([
            'module_id' => $user->id,
            'name' => 'Administrators',
            'module_name' => 'admin',
            'access' => '[1,2,3,4]',
            'route' => 'admin-profiles.index',
            'icon' => 'fas fa-users-cog'
        ]);

        Module::create([
            'module_id' => $user->id,
            'name' => 'Requestor',
            'module_name' => 'requestor',
            'access' => '[1,2,3,4]',
            'route' => 'requestor-profiles.index',
            'icon' => 'fas fa-users'
        ]);

        $role = Module::create([
            'module_id' => null,
            'name' => 'Role Management',
            'module_name' => 'role',
            'access' => null,
            'route' => null,
            'icon' => null
        ]);

        Module::create([
            'module_id' => $role->id,
            'name' => 'Access Control Level',
            'module_name' => 'access_control',
            'access' => '[1,2,3,4]',
            'route' => 'access-controls.index',
            'icon' => 'fas fa-gamepad'
        ]);

        Module::create([
            'module_id' => $role->id,
            'name' => 'Department',
            'module_name' => 'department',
            'access' => '[1,2,3,4]',
            'route' => 'departments.index',
            'icon' => 'fas fa-building'
        ]);

        Module::create([
            'module_id' => $role->id,
            'name' => 'Designation',
            'module_name' => 'designation',
            'access' => '[1,2,3,4]',
            'route' => 'designations.index',
            'icon' => 'fas fa-user-md'
        ]);

        $request = Module::create([
            'module_id' => null,
            'name' => 'Request Management',
            'module_name' => 'request',
            'access' => null,
            'route' => null,
            'icon' => null
        ]);

        Module::create([
            'module_id' => $request->id,
            'name' => 'Request Forms',
            'module_name' => 'request_forms',
            'access' => '[1,4]',
            'route' => 'request-forms.index',
            'icon' => 'fab fa-wpforms'
        ]);

        Module::create([
            'module_id' => $request->id,
            'name' => 'Forms',
            'module_name' => 'forms',
            'access' => '[1,2,3,4]',
            'route' => 'forms.index',
            'icon' => 'fas fa-book'
        ]);

        Module::create([
            'module_id' => $request->id,
            'name' => 'Emergency Request',
            'module_name' => 'emergency_request',
            'access' => '[1,4]',
            'route' => 'emergency-requests.index',
            'icon' => 'fas fa-ambulance'
        ]);
    }
}
