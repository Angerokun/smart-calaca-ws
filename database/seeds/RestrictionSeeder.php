<?php

use App\Restriction;
use Illuminate\Database\Seeder;

class RestrictionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Restriction::create([
            'name' => 'view only'
        ]);

        Restriction::create([
            'name' => 'view/create'
        ]);

        Restriction::create([
            'name' => 'view/update'
        ]);
        
        Restriction::create([
            'name' => 'view/delete'
        ]);

        Restriction::create([
            'name' => 'view/print'
        ]);
    }
}
