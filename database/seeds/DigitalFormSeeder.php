<?php

use App\DigitalForm;
use Illuminate\Database\Seeder;

class DigitalFormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DigitalForm::create([
            'name' => 'Birth Certificate Form',
            'url' => 'http://132.148.19.222/smart-calaca-ws/resources/forms/Birth_Application_Form.pdf',
            'file' => null
        ]);

        DigitalForm::create([
            'name' => 'Assessor Application',
            'url' => 'http://132.148.19.222/smart-calaca-ws/resources/forms/Assessor.docx',
            'file' => null
        ]);

        DigitalForm::create([
            'name' => 'Business Permit Form',
            'url' => 'http://132.148.19.222/smart-calaca-ws/resources/forms/BPLO.docx',
            'file' => null
        ]);

        DigitalForm::create([
            'name' => 'APPLICATION FOR LOCATIONAL CLEARANCE/CERTIFICATE OF ZONING COMPLIANCE',
            'url' => 'http://132.148.19.222/smart-calaca-ws/resources/forms/Planning.docx',
            'file' => null
        ]);
    }
}
