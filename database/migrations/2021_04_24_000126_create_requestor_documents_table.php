<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestorDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requestor_documents', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('requested_id');
            $table->unsignedBigInteger('user_id');
            $table->text('remarks')->nullable();
            $table->string('file_name');
            $table->text('file');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('requested_id')
                ->references('id')
                ->on('requested_forms')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requestor_documents');
    }
}
