<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminRequestActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_request_activities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('requested_id');
            $table->unsignedBigInteger('assigned_to')->nullable();
            $table->text('remarks');
            $table->string('subject');
            $table->string('file_name')->nullable();
            $table->text('file')->nullable();
            $table->string('priority');
            $table->integer('update_user')->default(0);
            $table->text('update_info')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('requested_id')
                ->references('id')
                ->on('requested_forms')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            
            $table->foreign('assigned_to')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_request_activities');
    }
}
