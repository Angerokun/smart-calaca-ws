<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestedFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requested_forms', function (Blueprint $table) {
            $table->id();
            $table->string('requested_no')->unique();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('form_id');
            $table->string('status')->nullable();
            $table->text('description')->nullable();
            $table->string('priority')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('form_id')
                ->references('id')
                ->on('forms')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requested_forms');
    }
}
