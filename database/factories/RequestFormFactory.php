<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\RequestForm;
use Faker\Generator as Faker;

$factory->define(RequestForm::class, function (Faker $faker) {
    return [
        'requested_no' => $faker->numberBetween(2520, 90000),
        'user_id' => 2,
        'form_id' => 1,
        'status' => $faker->randomElement(RequestForm::STATUS),
        'description' => $faker->text(20),
        'priority' => $faker->randomElement(RequestForm::PRIORITY),
    ];
});
