<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->middleware(['guest']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get(
    '/digital-forms/birth-certificate/create/{id}',
    'DigitalFormController@birthCertificate'
);

/* Admin Routes */
Route::group(
    ['prefix' => 'admin',  'middleware' => 'auth'],
    function () {
        Route::get(
            '/admin-profiles/edit-account/{id}',
            'AdministratorController@editAccount'
        )->name('account.edit');

        Route::put(
            '/admin-profiles/update-account/{id}',
            'AdministratorController@updateAccount'
        )->name('account.update');

        Route::get(
            '/admin-profile-list',
            'AdministratorController@adminProfileDatatable'
        )->name('admin-profile-list');

        Route::get(
            '/admin-profiles/delete/{id}',
            'AdministratorController@delete'
        )->name('admin-profiles.delete');

        Route::resource('admin-profiles', 'AdministratorController');
        
        Route::get(
            '/requestor-profile-list',
            'RequestorController@requestorDatatable'
        )->name('requestor-profile-list');

        Route::get(
            '/requestor-profiles/delete/{id}',
            'RequestorController@delete'
        )->name('requestor-profiles.delete');

        Route::resource('requestor-profiles', 'RequestorController');

        Route::get(
            '/access-control-list',
            'AccessControlController@accessControlDatatable'
        )->name('access-control-list');

        Route::get(
            '/access-controls/delete/{id}',
            'AccessControlController@delete'
        )->name('access-controls.delete');

        Route::resource('access-controls', 'AccessControlController');

        Route::get(
            '/department-list',
            'DepartmentController@departmentDatatable'
        )->name('department-list');

        Route::get(
            '/departments/delete/{id}',
            'DepartmentController@delete'
        )->name('departments.delete');

        Route::resource('departments', 'DepartmentController');

        Route::get(
            '/designation-list',
            'DesignationController@designationtDatatable'
        )->name('designation-list');

        Route::get(
            '/designations/delete/{id}',
            'DesignationController@delete'
        )->name('designations.delete');

        Route::get(
            '/form-list',
            'FormController@formDatatable'
        )->name('form-list');

        Route::get(
            '/forms/delete/{id}',
            'FormController@delete'
        )->name('forms.delete');

        Route::resource('forms', 'FormController');

        Route::resource('designations', 'DesignationController');

        Route::get(
            '/request-form-list',
            'RequestFormController@requestFormDatatable'
        )->name('request-form-list');

        Route::get(
            '/request-forms/delete/{id}',
            'RequestFormController@delete'
        )->name('request-forms.delete');

        Route::get(
            '/download-docs/{docId}',
            'RequestFormController@downloadDocs'
        )->name('download-docs');

        Route::resource('request-forms', 'RequestFormController');

        Route::get(
            '/download-docs-activities/{docId}',
            'AdminRequestActivityController@downloadDocs'
        )->name('download-docs-activities');

        Route::get(
            '/admin-request-activities/{requestedId}',
            'AdminRequestActivityController@create'
        )->name('admin-request-activities.create');

        Route::get(
            '/admin-request-activities/{requestedId}/activities/{activityId}',
            'AdminRequestActivityController@show'
        )->name('admin-request-activities.show');

        Route::post(
            '/admin-request-activities/{requestedId}',
            'AdminRequestActivityController@store'
        )->name('admin-request-activities.store');

        Route::get(
            '/emergency-request-list',
            'EmergencyRequestController@emergencyRequestDatatable'
        )->name('emergency-request-list');

        Route::get(
            '/emergency-requests/delete/{id}',
            'EmergencyRequestController@delete'
        )->name('emergency-requests.delete');

        Route::resource('emergency-requests', 'EmergencyRequestController');

        Route::get(
            '/notification-list',
            'NotificationController@notificationDatatable'
        )->name('notification-list');

        Route::get('notifications', 'NotificationController@index')->name('notification.index');
    }
);

Route::get('notification/mark-read', 'NotificationController@readAllNotification')->name('markAsRead');

Route::put('notification/{id}', 'NotificationController@markAsViewNotification')->name('markAsView');
