<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('register', 'Api\AuthController@register');
    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::get('user-profile', 'Api\AuthController@userProfile');
        Route::get('logout', 'Api\AuthController@logout');
    });
});

Route::group(
    ['prefix' => 'request-docs', 'middleware' => 'auth:api'],
    function () {
        Route::post('store/{userId}', 'Api\RequestFormController@store');
    }
);

Route::get('download-docs-activities/{docId}', 'AdminRequestActivityController@downloadDocs');

Route::group(
    ['prefix' => 'notifications', 'middleware' => 'auth:api'],
    function () {
        Route::get('', 'Api\NotificationController@getAllUserNotification');
        Route::get('mark-read', 'Api\NotificationController@readAllNotification');
        Route::put('{notifId}', 'Api\NotificationController@markAsViewNotification');
    }
);

Route::group(
    ['prefix' => 'request-docs', 'middleware' => 'auth:api'],
    function () {
        Route::group(
            ['prefix' => '{userId}'],
            function () {
                Route::post('/', 'Api\RequestFormController@store');
                Route::get('/', 'Api\RequestFormController@index');

                Route::group(
                    ['prefix' => 'requested'],
                    function () {
                        Route::get('/{reqId}', 'Api\RequestFormController@show');
                    }
                );

                Route::group(
                    ['prefix' => 'documents'],
                    function () {
                        Route::post('/{docId}', 'Api\RequestorDocumentController@store');
                        Route::get('/{docId}', 'Api\RequestorDocumentController@index');
                    }
                );
            }
        );
    }
);

Route::group(
    ['prefix' => 'forms', 'middleware' => 'auth:api'],
    function () {
        Route::get('/', 'Api\FormController@index');
        Route::get('/{formId}', 'Api\FormController@show');
    }
);

Route::group(
    ['prefix' => 'emergency-requests', 'middleware' => 'auth:api'],
    function () {
        Route::group(
            ['prefix' => '{userId}'],
            function () {
                Route::post('/', 'Api\EmergencyRequestController@store');
                Route::get('/', 'Api\EmergencyRequestController@index');
                Route::get('requests/{id}', 'Api\EmergencyRequestController@show');
            }
        );
    }
);

Route::group(
    ['prefix' => 'hotlines', 'middleware' => 'auth:api'],
    function () {
        Route::get('/', 'Api\EmergencyRequestController@hotlines');
        Route::get('/{hotlineId}', 'Api\EmergencyRequestController@showHotline');
    }
);
