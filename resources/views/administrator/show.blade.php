@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Administrator</h1>
<p class="mb-4">Administrator are the one who access Web Portal for SMART CALACA.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div align='right' style="margin-top: -21px;">
            <a href="{{ route('admin-profiles.index') }}">
                <button type="button" style="margin-bottom: -44px;" class="btn btn-default">
                <i class="fas fa-chevron-left"></i> Back</button
            ></a>
        </div>
        <h6 class="m-0 font-weight-bold text-primary">
            {{ $adminProfile->last_name }}, {{ $adminProfile->first_name }} - {{ $adminProfile->user->name }}
        </h6>
    </div>
    
    <div class="card-body">
        <!-- Nested Row within Card Body -->
        <div class="errors"></div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>First Name</b></label><br>
                        <p>&nbsp;{{ $adminProfile->first_name }}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Middle Name</b></label><br>
                        <p>&nbsp;{{ $adminProfile->middle_name }}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Last Name</b></label><br>
                        <p>&nbsp;{{ $adminProfile->last_name }}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Designation</b></label><br>
                        <p>&nbsp;{{ $adminProfile->designation->name }} - {{ $adminProfile->designation->department->name }}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Birth Date</b></label><br>
                        <p>&nbsp;{{ date('F d, Y', strtotime($adminProfile->birth_date)) }}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Contact No.</b></label><br>
                        <p>&nbsp;{{ ($adminProfile->contact_no != null ? $adminProfile->contact_no : 'N/A')  }}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <h3>Account Information</h3>
                <hr>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>User Name</b></label><br>
                        <p>&nbsp;{{ $adminProfile->user->name }}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Email</b></label><br>
                        <p>&nbsp;{{ $adminProfile->user->email }}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Access Control</b></label><br>
                        <p>&nbsp;{{ $userAccess->userPermission->name }} - ({{ ucwords($userAccess->userPermission->user_type) }})</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection