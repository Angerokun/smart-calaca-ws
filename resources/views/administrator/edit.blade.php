@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Administrator</h1>
<p class="mb-4">Administrator are the one who access Web Portal for SMART CALACA.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div align='right' style="margin-top: -21px;">
            <a href="{{ route('admin-profiles.index') }}">
                <button type="button" style="margin-bottom: -44px;" class="btn btn-default">
                <i class="fas fa-chevron-left"></i> Back</button
            ></a>
        </div>
        <h6 class="m-0 font-weight-bold text-primary">Edit Administrator Profile</h6>
    </div>
    
    <div class="card-body">
        <!-- Nested Row within Card Body -->
        <div class="errors"></div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="p-5">
                    <div class="text-center">
                        <h2 class="h4 text-gray-900 mb-4">Basic Information</h2>
                    </div>
                    <form class="user" method="POST" id ="admin-edit" action="{{ route('admin-profiles.update', $adminProfile->id) }}">
                        <input type="hidden" name="_method" value="PUT">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control form-control" id="exampleFirstName"
                                    placeholder="First Name" data-validation="required"
                                    name="first_name" value="{{ $adminProfile->first_name }}"
                                    style="border-right: solid 4px #f6c23e">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control form-control" id="exampleFirstName"
                                    name="middle_name" value="{{ $adminProfile->middle_name }}"
                                    placeholder="Middle Name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control form-control" id="exampleFirstName"
                                    placeholder="Last Name" data-validation="required"
                                    name="last_name" value="{{ $adminProfile->last_name }}"
                                    style="border-right: solid 4px #f6c23e">
                        </div>
                        <div class="form-group">
                            <select type="text" class="form-control form-control" 
                                style="border-right: solid 4px #f6c23e" name="designation_id" data-validation="required">
                                <option value="">Select Designation</option>
                                @foreach ($designations as $designation)
                                    <option value="{{ $designation->id }}" 
                                        {{($designation->id == $adminProfile->designation_id ? 'selected': '' )}}>
                                        {{ $designation->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control form-control" id="exampleFirstName"
                                    placeholder="Contact No." data-validation="required"
                                    name="contact_no" value="{{ $adminProfile->contact_no }}"
                                    style="border-right: solid 4px #f6c23e">
                        </div>
                        <div class="form-group">
                            <input type="date" class="form-control form-control" id="exampleFirstName"
                                    placeholder="Birth Date" data-validation="required"
                                    name="birth_date" value="{{ $adminProfile->birth_date }}"
                                    style="border-right: solid 4px #f6c23e">
                        </div>
                        <div class="text-center">
                            <h2 class="h4 text-gray-900 mb-4">Account Information</h2>
                        </div>
                        <hr>
                        <a href="{{ route('account.edit', $adminProfile->id) }}"><button type="button" class="btn btn-secondary"><i class="fas fa-edit"></i> Edit Account</button></a>
                        <div class="text-right">
                            <button type="submit" class="btn btn-success btn-icon-split">
                                <span class="icon text-white-50">
                                    <i class="fas fa-edit"></i>
                                </span>
                                <span class="text">Update</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $('#admin-edit').register_fields('.errors');
    </script>
@endpush
@endsection