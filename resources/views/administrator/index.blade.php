@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Administrator</h1>
<p class="mb-4">Administrator are the one who access Web Portal for SMART CALACA.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Administrator List</h6>
    </div>
    <div class="card-body">
        <div align='right' style="margin-bottom: 20px;">
            <a href="{{ route('admin-profiles.create') }}" class="btn btn-primary btn-icon-split">
                <span class="icon text-white-50">
                    <i class="fas fa-plus"></i>
                </span>
                <span class="text">New</span>
            </a>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered" id="admin-table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Department</th>
                        <th>Contact No.</th>
                        <th>Birth Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@push('scripts')
<script>
        $(function() {
            $('#admin-table').DataTable(
                {
                    responsive: true,
                    pagingType: "full_numbers",
                    processing: true,
                    serverSide: true,
                    type: "GET",
                    ajax:"{!! route('admin-profile-list') !!}",
                    columns:[
                        {data: "name","name":"name","width":"22%",
                            "render": function (data, type, row, meta) {
                                var url = '{{ route("admin-profiles.show", ":id") }}';
                                url = url.replace(':id', row.id);
                                return "<a href='"+url+"'>"+data+"</a>"; 
                        }},
                        {data: "position","name":"position","width":"20%"},
                        {data: "department","name":"department","width":"20%"},
                        {data: "contact_no","name":"contact_no","width":"15%"},
                        {data: "birth_date","name":"birth_date","width":"15%"},
                        {data: "action","name":"action",orderable:false,searchable:false,"width":"8%"},
                    ]
                }
            );
        });
    </script>
    <!-- Page level custom scripts -->
    <script src="{{ asset('web-design/js/demo/datatables-demo.js') }}"></script>
@endpush
@endsection