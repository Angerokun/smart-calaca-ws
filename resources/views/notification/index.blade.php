@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Notifications</h1>
<p class="mb-4">List all Notification</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Notification List</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="notification-table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Date Created</th>
                        <th>Description</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $(function() {
            $('#notification-table').DataTable(
                {
                    responsive: true,
                    pagingType: "full_numbers",
                    processing: true,
                    serverSide: true,
                    type: "GET",
                    ajax:"{!! route('notification-list') !!}",
                    columns:[
                        {data: "date","name":"date","width":"20%",
                            "render": function (data, type, row, meta) {
                                return "<a href='"+row.route+"'>"+data+"</a>"; 
                        }},
                        {data: "description","name":"description","width":"80%"},
                    ]
                }
            );
        });
    </script>
    <!-- Page level custom scripts -->
    <script src="{{ asset('web-design/js/demo/datatables-demo.js') }}"></script>
@endpush
@endsection