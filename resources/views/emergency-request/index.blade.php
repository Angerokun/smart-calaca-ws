@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Emergency Request</h1>
<p class="mb-4">Citizen Emergency Request List.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Emergency Request List</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="emergency-request-table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Department</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Contact Number</th>
                        <th>Area of Incident</th>
                        <th>Action</th>

                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $(function() {
            $('#emergency-request-table').DataTable(
                {
                    order: [0, 'desc'],
                    responsive: true,
                    pagingType: "full_numbers",
                    processing: true,
                    serverSide: true,
                    type: "GET",
                    ajax:"{!! route('emergency-request-list') !!}",
                    columns:[
                        {data: "date","name":"date","width":"15%"},
                        {data: "department","name":"department","width":"19%",
                            "render": function (data, type, row, meta) {
                                var url = '{{ route("emergency-requests.show", ":id") }}';
                                url = url.replace(':id', row.id);
                                return "<a href='"+url+"'>"+data+"</a>"; 
                        }},
                        {data: "name","name":"name","width":"18%"},
                        {data: "address","name":"address","width":"15%"},
                        {data: "contact_number","name":"contact_number","width":"15%"},
                        {data: "area_incident","name":"area_incident","width":"15%"},
                        {data: "action","name":"action",orderable:false,searchable:false,"width":"8%"},
                    ]
                }
            );
        });
    </script>
    <!-- Page level custom scripts -->
    <script src="{{ asset('web-design/js/demo/datatables-demo.js') }}"></script>
@endpush
@endsection