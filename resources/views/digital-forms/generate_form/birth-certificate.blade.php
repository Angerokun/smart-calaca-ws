<table style="border: 0.5px solid black; width: 100%; height:100%">
    <tr style="border: 0.5px solid black;"> 
        <td style="border: 0.5px solid black; width: 30%;">
            <p class="birth_certificate">&#10003;</p>
        </td>
        <td style="border: 0.5px solid black; width: 30%;">
            <p class="authentication"></p>
        </td>
        <td style="border: 0.5px solid black; width: 30%;">
            <p class="cdli"></p>
        </td>
    </tr>
</table>
<table style="border: 0.5px solid black; width: 100%; height:100%">

    <tr style="border: 0.5px solid black;"> 
        <td style="border: 0.5px solid black;">
            <p class="number_one">&#10003;</p>
        </td>
        <td style="border: 0.5px solid black;">
            <p class="number_two"></p>
        </td>
        <td style="border: 0.5px solid black;">
            <p class="number_specify"></p>
        </td>
    </tr>
</table>
<table style="border: 0.5px solid black; width: 100%; height:100%">
    <tr style="border: 0.5px solid black;">
        <td style="border: 0.5px solid black; width: 70%">
            <p class="birth_ref_no"></p>
        </td>
        <td style="border: 0.5px solid black; width: 30%">
            <div>
                <p class="gender_MALE">&#10003;</p>
                <p class="gender_FEMALE"></p>
            </div>
        </td>
    </tr>
</table>
<table style="border: 0.5px solid black; width: 100%; height:100%">
    <tr style="border: 0.5px solid black;">
        <td style="border: 0.5px solid black;">
            <p class="last_name">{{$requestor->last_name}}</p>
        </td>
    </tr>
    <tr style="border: 0.5px solid black;">
        <td style="border: 0.5px solid black;">
            <p class="first_name">{{ $requestor->first_name }}</p>
        </td>
    </tr>
    <tr style="border: 0.5px solid black;">
        <td style="border: 0.5px solid black;">
            <p class="middle_name">{{ $requestor->middle_name }}</p>
        </td>
    </tr>
    <tr style="border: 0.5px solid black;">
        <td style="border: 0.5px solid black;">
            <p class="birth_date">{{date('F d Y', strtotime($requestor->birth_date))}}</p>
        </td>
    </tr>
    <tr style="border: 0.5px solid black;">
        <td style="border: 0.5px solid black;">
            <p class="city"></p>
        </td>
    </tr>
    <tr style="border: 0.5px solid black;">
        <td style="border: 0.5px solid black;">
            <p class="province"></p>
        </td>
    </tr>
    <tr style="border: 0.5px solid black;">
        <td style="border: 0.5px solid black;">
            <p class="specify_born_abroad"></p>
        </td>
    </tr>
    <tr style="border: 0.5px solid black;">
        <td style="border: 0.5px solid black;">
            <p class="father_last_name"></p>
        </td>
    </tr>
    <tr style="border: 0.5px solid black;">
        <td style="border: 0.5px solid black;">
            <p class="father_first_name"></p>
        </td>
    </tr>
    <tr style="border: 0.5px solid black;">
        <td style="border: 0.5px solid black;">
            <p class="father_middle_name"></p>
        </td>
    </tr>
    <tr style="border: 0.5px solid black;">
        <td style="border: 0.5px solid black;">
            <p class="mother_last_name"></p>
        </td>
    </tr>
    <tr style="border: 0.5px solid black;">
        <td style="border: 0.5px solid black;">
            <p class="mother_first_name"></p>
        </td>
    </tr>
    <tr style="border: 0.5px solid black;">
        <td style="border: 0.5px solid black;">
            <p class="mother_middle_name"></p>
        </td>
    </tr>
</table>
<table style="border: 0.5px solid black; width: 100%; height:100%">
    <tr style="border: 0.5px solid black;">
        <td style="border: 0.5px solid black; width:50%">
            <p class="presented_YES">&#10003;</p>
        </td>
        <td style="border: 0.5px solid black; width:50%">
            <p class="presented_NO"></p>
        </td>
    </tr>
</table>
