<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SMART CALACA | POWER TOWN, POWER FROM THE PEOPLE</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/all.js') }}"></script>
    <!-- <script src="{{ asset('web-design/vendor/jquery/jquery.min.js') }}"></script> -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/all.css') }}" >

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('web-design/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('web-design/css/sb-admin-2.css') }}" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('web-design/img/favicon.png') }}">
    <style>
        table, th, td {
            border: 1px solid black;
        }
        table.center {
            margin-left: auto;
            margin-right: auto;
        }
    </style>
</head>
<body class="bg-gradient-primary">
    <div id="app">
        <div class="container-fluid">
            
            <!-- Page Heading -->
            <h1 class="h3 mb-2 " align="center" style="color: #ffff;">OFFICIAL OF THE CIVIL REGISTRAR GENERAL APPLICATION FORM - BIRTH CERTIFICATE</h1>
            <!-- <p class="mb-4" align="center" style="color: #ffff;">Department.</p> -->

            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">DIGITAL APPLICATION FORM - BIRTH CERTIFICATE</h6>
                </div>
                
                <div class="card-body">
                    <!-- Nested Row within Card Body -->
                    <div class="errors"></div>
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            <div class="p-5">
                                <form class="user" method="POST" id ="birth-certificate-create" action="">
                                    @csrf
                                    <div class="form-group">
                                        <select class="form-control" name="request_for" placeholder="Request for" id="request_for">
                                            <option value="">Request For</option>
                                            <option value="BIRTH CERTIFICATE">BIRTH CERTIFICATE</option>
                                            <option value="AUTHENTICATION">AUTHENTICATION</option>
                                            <option value="CD/LI">CD/LI</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"
                                                placeholder="Number of copies" data-validation="required" name="name" id="number_of_copies"
                                                style="border-right: solid 4px #f6c23e">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"
                                                placeholder="Birth Reference No." data-validation="required" name="name" id="birth_ref_no"
                                                style="border-right: solid 4px #f6c23e">
                                    </div>
                                    <div class="form-group">
                                        <p>Select Gender:</p>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gender" id="exampleRadios1" value="MALE">
                                            <label class="form-check-label" for="gender">
                                                Male
                                            </label>
                                            </div>
                                            <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gender" id="exampleRadios2" value="FEMALE">
                                            <label class="form-check-label" for="gender">
                                                Female
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control"
                                                placeholder="Last Name" data-validation="required" name="name"
                                                style="border-right: solid 4px #f6c23e" id="last_name"
                                                value="{{$requestor->last_name}}">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control"
                                                placeholder="First Name" data-validation="required" name="name"
                                                style="border-right: solid 4px #f6c23e" id="first_name"
                                                value="{{$requestor->first_name}}">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control"
                                                placeholder="Middle Name" data-validation="required" name="name"
                                                value="{{$requestor->middle_name}}" id="middle_name">
                                    </div>
                                    <div class="form-group">
                                        <input type="date" class="form-control form-control"
                                                placeholder="Date of Birth" data-validation="required" name="name"
                                                style="border-right: solid 4px #f6c23e" id="birth_date"
                                                value="{{date('Y-m-d', strtotime($requestor->birth_date))}}">
                                    </div>
                                    <hr><p>Place of Birth</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control" id="city"
                                                placeholder="City/ Municipality" data-validation="required" name="name"
                                                style="border-right: solid 4px #f6c23e">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control" id="province"
                                                placeholder="Province" data-validation="required" name="name"
                                                style="border-right: solid 4px #f6c23e">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control" id="specify_born_abroad"
                                                placeholder="Please specify country if born abroad only:" data-validation="required" name="name">
                                    </div>
                                    <hr><p>NAME OF FATHER</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control" id="father_last_name"
                                                placeholder="Last Name" data-validation="required" name="name"
                                                style="border-right: solid 4px #f6c23e">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control" id="father_first_name"
                                                placeholder="First Name" data-validation="required" name="name"
                                                style="border-right: solid 4px #f6c23e">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control" id="father_middle_name"
                                                placeholder="Middle Name" data-validation="required" name="name">
                                    </div>
                                    <hr><p>MAIDEN NAME OF MOTHER</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control" id="mother_last_name"
                                                placeholder="Last Name" data-validation="required" name="name"
                                                style="border-right: solid 4px #f6c23e">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control" id="mother_first_name"
                                                placeholder="First Name" data-validation="required" name="name"
                                                style="border-right: solid 4px #f6c23e">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control" id="mother_middle_name"
                                                placeholder="Middle Name" >
                                    </div>
                                    <h2></h2>
                                    <div class="form-group">
                                        <p>PRESENTED VALID IDs AND AUTHORIZATION LETTER?</p>
                                        <div class="form-check">
                                            <input class="form-check-input presented" type="radio" name="presented" value="YES">
                                            <label class="form-check-label" for="exampleRadios1">
                                                YES
                                            </label>
                                            </div>
                                            <div class="form-check">
                                            <input class="form-check-input presented" type="radio" name="presented" value="NO">
                                            <label class="form-check-label" for="exampleRadios2">
                                                NO
                                            </label>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <button class="btn btn-success btn-icon-split print-form-report" data-form="birth-certificate-form">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-save"></i>
                                            </span>
                                            <span class="text">GENERATE FORM</span>
                                        </button>
                                    </div>
                                    <div id="birth-certificate-form"  hidden>
                                        @include('digital-forms.generate_form.birth-certificate')
                                        <style>
                                            .mt-table{
                                                margin-top: 1.5rem!important;
                                            }
                                            .bbm-supply {
                                                border-bottom: 2px solid;
                                                position: inherit;
                                                margin-bottom: -20px;
                                                margin-top: -1rem;
                                                width: 80%;
                                            }
                                        </style>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#birth-certificate-create').register_fields('.errors');
        $('#request_for').on('input', function(){ 
            $request_for = $('#request_for').val();
            $check = '<span class="icon text-black-50"><i class="fas fa-check"></i></span>';
            switch ($request_for) {
                case "BIRTH CERTIFICATE":
                    $('.cdli').html('');
                    $('.authentication').html('');
                    $('.birth_certificate').html('&#10003;');
                    break;
                case "AUTHENTICATION":
                    $('.birth_certificate').html('');
                    $('.cdli').html('');
                    $('.authentication').html('&#10003;');
                    break;
                case "CD/LI":
                    $('.birth_certificate').html('');
                    $('.authentication').html('');
                    $('.cdli').html('&#10003;');
                    break;
                default:
                    break;
            }
        });
        $('input[type=radio][name=presented]').change(function(){
            $('.presented_'+this.value).html('&#10003;');
            if(this.value == "YES"){
                $('.presented_YES').html('&#10003;');
                $('.presented_NO').html('');
            }else{
                $('.presented_YES').html('');
                $('.presented_NO').html('&#10003;');
            }
        });
        $('#number_of_copies').on('input', function(){ 
            if(this.value > 2){
                $('.number_specify').text($('#number_of_copies').val());
                $('.number_two').text('');
                $('.number_one').text('');
            }else{
                if(this.value == 1){
                    $('.number_one').text($('#number_of_copies').val());
                    $('.number_specify').text('');
                    $('.number_two').text('');
                }else{
                    $('.number_two').text($('#number_of_copies').val());
                    $('.number_one').text('');
                    $('.number_specify').text('');
                }
            }
        });
        $('input[type=radio][name=gender]').change(function(){
            $('.presented_'+this.value).html('&#10003;');
            if(this.value == "YES"){
                $('.gender_MALE').html('&#10003;');
                $('.gender_FEMALE').html('');
            }else{
                $('.gender_MALE').html('');
                $('.gender_FEMALE').html('&#10003;');
            }
        });
        $('#birth_ref_no').on('input', function(){ $('.birth_ref_no').text($('#birth_ref_no').val().toUpperCase()); });
        $('#first_name').on('input', function(){ $('.first_name').text($('#first_name').val().toUpperCase()); });
        $('#last_name').on('input', function(){ $('.last_name').text($('#last_name').val().toUpperCase()); });
        $('#middle_name').on('input', function(){ $('.middle_name').text($('#middle_name').val().toUpperCase()); });
        $('#birth_date').on('input', function(){ $('.birth_date').text($('#birth_date').val().toUpperCase()); });
        $('#city').on('input', function(){ $('.city').text($('#city').val().toUpperCase()); });
        $('#province ').on('input', function(){ $('.province ').text($('#province ').val().toUpperCase()); });
        $('#specify_born_abroad ').on('input', function(){ $('.specify_born_abroad ').text($('#specify_born_abroad ').val().toUpperCase()); });
        $('#father_last_name ').on('input', function(){ $('.father_last_name ').text($('#father_last_name ').val().toUpperCase()); });
        $('#father_first_name ').on('input', function(){ $('.father_first_name ').text($('#father_first_name ').val().toUpperCase()); });
        $('#father_middle_name ').on('input', function(){ $('.father_middle_name ').text($('#father_middle_name ').val().toUpperCase()); });
        $('#mother_last_name ').on('input', function(){ $('.mother_last_name ').text($('#mother_last_name ').val().toUpperCase()); });
        $('#mother_first_name ').on('input', function(){ $('.mother_first_name ').text($('#mother_first_name ').val().toUpperCase()); });
        $('#mother_middle_name ').on('input', function(){ $('.mother_middle_name ').text($('#mother_middle_name ').val().toUpperCase()); });


    </script>
    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('web-design/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{ asset('web-design/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{ asset('web-design/js/sb-admin-2.min.js') }}"></script>

</body>
</html>
