@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Request Forms</h1>
<p class="mb-4">Listed Below all the Requested Forms by Citizen/Requestor.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Requested Form List</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="request-form-table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Request No.</th>
                        <th>Requestor</th>
                        <th>Requested Form</th>
                        <th>Status</th>
                        <th>Priority</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $(function() {
            $('#request-form-table').DataTable(
                {
                    responsive: true,
                    order: [ 0, "desc" ],
                    pagingType: "full_numbers",
                    processing: true,
                    serverSide: true,
                    type: "GET",
                    ajax:"{!! route('request-form-list') !!}",
                    columns:[
                        {data: "date_created","name":"date_created","width":"12%"},
                        {data: "requested_no","name":"requested_no","width":"20%",
                            "render": function (data, type, row, meta) {
                                var url = '{{ route("request-forms.show", ":id") }}';
                                url = url.replace(':id', row.id);
                                return "<a href='"+url+"'>"+data+"</a>"; 
                        }},
                        {data: "requestor","name":"requestor","width":"20%"},
                        {data: "requested_form","name":"requested_form","width":"20%"},
                        {data: "status","name":"status","width":"12%"},
                        {data: "priority","name":"priority","width":"8%"},
                        {data: "action","name":"action",orderable:false,searchable:false,"width":"8%"},
                    ]
                }
            );
        });
    </script>
    <!-- Page level custom scripts -->
    <script src="{{ asset('web-design/js/demo/datatables-demo.js') }}"></script>
@endpush
@endsection