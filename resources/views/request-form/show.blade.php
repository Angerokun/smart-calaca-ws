@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Request Forms Information</h1>

<div class="row">

    <div class="col-lg-6">

        <!-- Circle Buttons -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div align='right' style="margin-top: -21px;">
                    <a href="{{ route('request-forms.index') }}">
                        <button type="button" style="margin-bottom: -44px;" class="btn btn-default">
                        <i class="fas fa-chevron-left"></i> Back</button
                    ></a>
                </div>
                <h6 class="m-0 font-weight-bold text-primary">Requestor Information</h6>
            </div>
            <div class="card-body">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label><b>Name</b></label><br>
                                <p>&nbsp;{{ $requestForm->user->user->first_name }} {{ $requestForm->user->user->last_name }}</p>
                            </div>
                            <div class="form-group col-md-6">
                            <label><b>Date Requested</b></label><br>
                                <p>&nbsp;{{ date('F d, Y', strtotime($requestForm->created_at)) }}</p>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label><b>Requested Form</b></label><br>
                                <p>&nbsp;{{ $requestForm->form->name }}</p>
                            </div>
                            <div class="form-group col-md-6">
                                <label><b>Priority</b></label><br>
                                @if ($requestForm->priority == 'low')
                                <span class="badge badge-pill badge-light">{{ ucwords($requestForm->priority) }}</span>
                                @elseif ($requestForm->priority == 'medium')
                                <span class="badge badge-pill badge-warning">{{ ucwords($requestForm->priority) }}</span>
                                @else
                                <span class="badge badge-pill badge-danger">{{ ucwords($requestForm->priority) }}</span>
                                @endIf
                            </div>
                            <div class="form-group col-md-6">
                                <label><b>Status</b></label><br>
                                <span class="badge badge-pill badge-primary">{{ ucwords(str_replace('_', ' ', $requestForm->status)) }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Brand Buttons -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Attachments</h6>
            </div>
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th scope="col">File Name</th>
                        <th scope="col">Remarks</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($requestForm->documents) > 0)
                            @foreach($requestForm->documents as $document)
                            <tr>
                            <td>{{ $document->file_name }}</td>
                            <td>{{ $document->remarks != null ? $document->remarks : 'N/A' }}</td>
                            <td><a href="{!! route('download-docs', $document->id) !!}">Download</a></td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <div class="col-lg-6">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Activities</h6>
            </div>
            <div class="card-body">
                <h4 class="small font-weight-bold">Activity Progress <span
                        class="float-right">{{ $percent }}%</span></h4>
                <div class="progress mb-4">
                    <div class="progress-bar" role="progressbar" style="width: {{ $percent }}%"
                        aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                @if ($requestForm->activities)
                <div class="list-group">
                    @foreach ($requestForm->activities()->orderBy('id', 'DESC')->get() as $activity)
                    <a href="" data-toggle="modal"
                        title="{{ $activity->subject }}"
                        data-url="{{ route('admin-request-activities.show', [$requestForm->id, $activity->id]) }}" 
                        class="list-group-item list-group-item-action flex-column align-items-start popup_form">
                        <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">{{ $activity->subject }}</h5>
                        <small>{{ date('F d, Y', strtotime($activity->created_at)) }}</small>
                        </div>
                        <p class="mb-1">{{ $activity->remarks }}</p>
                        <small></small>
                    </a>
                    @endforeach
                </div>
                @endif
                <a data-url="{{ route('admin-request-activities.create', $requestForm->id) }}" 
                    title="Add Activity" 
                    class="btn btn-primary popup_form"
                    data-toggle="modal"
                    style="margin-top: 15px;"
                    >Add Activity</a>
            </div>
        </div>

    </div>

</div>

</div>
<!-- /.container-fluid -->
@endsection