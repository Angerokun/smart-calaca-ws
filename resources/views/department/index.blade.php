@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Department</h1>
<p class="mb-4">Department on the Municipality Record.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Department List</h6>
    </div>
    <div class="card-body">
        <div align='right' style="margin-bottom: 20px;">
            <a href="{{ route('departments.create') }}" class="btn btn-primary btn-icon-split">
                <span class="icon text-white-50">
                    <i class="fas fa-plus"></i>
                </span>
                <span class="text">New</span>
            </a>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered" id="department-table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $(function() {
            $('#department-table').DataTable(
                {
                    responsive: true,
                    pagingType: "full_numbers",
                    processing: true,
                    serverSide: true,
                    type: "GET",
                    ajax:"{!! route('department-list') !!}",
                    columns:[
                        {data: "name","name":"name","width":"50%",
                            "render": function (data, type, row, meta) {
                                var url = '{{ route("departments.show", ":id") }}';
                                url = url.replace(':id', row.id);
                                return "<a href='"+url+"'>"+data+"</a>"; 
                        }},
                        {data: "description","name":"description","width":"40%"},
                        {data: "action","name":"action",orderable:false,searchable:false,"width":"8%"},
                    ]
                }
            );
        });
    </script>
    <!-- Page level custom scripts -->
    <script src="{{ asset('web-design/js/demo/datatables-demo.js') }}"></script>
@endpush
@endsection