@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Department</h1>
<p class="mb-4">Department on the Municipality Record.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div align='right' style="margin-top: -21px;">
            <a href="{{ route('departments.index') }}">
                <button type="button" style="margin-bottom: -44px;" class="btn btn-default">
                <i class="fas fa-chevron-left"></i> Back</button
            ></a>
        </div>
        <h6 class="m-0 font-weight-bold text-primary">Edit Department</h6>
    </div>
    
    <div class="card-body">
        <!-- Nested Row within Card Body -->
        <div class="errors"></div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="p-5">
                    <form class="user" method="POST" id ="department-create" action="{{ route('departments.update', $department->id) }}">
                        <input type="hidden" name="_method" value="PUT">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control form-control" id="exampleFirstName"
                                    placeholder="Department Name" data-validation="required" name="name"
                                    style="border-right: solid 4px #f6c23e" value="{{ $department->name }}">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="description" 
                                placeholder="Add Department Description...">{{ $department->description }}</textarea>
                        </div>
                        <div class="form-group">
                            <p>Add Emergency Response?</p>
                            <div class="form-check">
                                <input class="form-check-input checkbox_check" type="checkbox" name="emergency" id="exampleRadios1" value="1" {{ $department->hotline ? 'checked' : '' }}>
                                <label class="form-check-label" for="exampleRadios1" >
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div id="hotlines">
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-success btn-icon-split">
                                <span class="icon text-white-50">
                                    <i class="fas fa-edit"></i>
                                </span>
                                <span class="text">Update</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $('#department-create').register_fields('.errors');
        $(document).ready(function() {
            if ($('input.checkbox_check').is(':checked')) {
                append();
            } else {
                $('#hotlines > div').remove();
            }
        });

        $('#exampleRadios1').click( function () {
            if ($('input.checkbox_check').is(':checked')) {
                append();
            } else {
                $('#hotlines > div').remove();
            }
        });

        function append()
        {
            $('#hotlines').append(''+
                            '<div class="form-group">'+
                                '<input type="text" class="form-control form-control" '+
                                    ' id="exampleFirstName" placeholder="Mobile Number Hotline" '+
                                    ' name="mobile_number" '+
                                    ' value="{{ $department->hotline ? $department->hotline->mobile_number : ''}}"'+
                                    ' style="border-right: solid 4px #f6c23e">'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<input type="text" class="form-control form-control" id="exampleFirstName"'+
                                        ' value="{{$department->hotline ? $department->hotline->telephone : '' }}"'+
                                        'placeholder="Telephone Hotline" data-validation="required" name="telephone"'+
                                        'style="border-right: solid 4px #f6c23e">'+
                            '</div>');
        }
    </script>
    <!-- Page level custom scripts -->
    <script src="{{ asset('web-design/js/demo/datatables-demo.js') }}"></script>
@endpush
@endsection