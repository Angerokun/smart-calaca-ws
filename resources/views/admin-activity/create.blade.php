<form method="POST" id="post_activity_form"  class="form-horizontal" action="{{ route('admin-request-activities.store', $requestedId) }}" enctype="multipart/form-data">
    @csrf
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="errors"></div>
            <div class="form-group">
                <select class="form-control"
                    data-validation="required" name="subject" style="border-right: solid 4px #f6c23e">
                    <option value="">Subject</option>
                    <option value="Checking">Checking</option>
                    <option value="Waiting">Waiting</option>
                    <option value="Pending">Pending</option>
                    <option value="Payment">Payment</option>
                    <option value="Approved">Approved</option>
                </select>
                <!-- <input type="text" class="form-control form-control"
                        placeholder="Subject" data-validation="required" name="subject"
                        style="border-right: solid 4px #f6c23e"> -->
            </div>
            <div class="form-group">
                <select class="form-control"
                    data-validation="required" name="remarks" style="border-right: solid 4px #f6c23e">
                    <option value="">Add Remarks</option>
                    <option value="Checking the form submitted">Checking the form submitted</option>
                    <option value="Waiting to upload the Form">Waiting to upload the form</option>
                    <option value="Request form still in process">Request form still in process</option>
                    <option value="Ready for payment the requested Form">Ready for payment the requested form</option>
                    <option value="Ready to claim the requested Form">Ready to claim the requested Form</option>
                </select>
                <!-- <textarea class="form-control" 
                    placeholder="Add Remarks" 
                    style="border-right: solid 4px #f6c23e"
                    name="remarks"
                    ></textarea> -->
            </div>
            <div class="form-group">
                <select class="form-control" name="assigned_to">
                    <option value="">Assigned To</option>
                    @foreach($users as $user)
                        <option value="{{ $user->user->id }}">({{ $user->designation->name }}){{ $user->user->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <select class="form-control" name="priority">
                    <option value="low" {{ $priority == 'low' ? 'selected' : '' }}>Low</option>
                    <option value="medium" {{ $priority == 'medium' ? 'selected' : '' }}>Medium</option>
                    <option value="high" {{ $priority == 'high' ? 'selected' : '' }}>High</option>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control" name="status">
                    <option value="">Update Status</option>
                    <option value="request" {{ $request_status == 'request' ? 'selected' : '' }}>Request</option>
                    <option value="assigned" {{ $request_status == 'assigned' ? 'selected' : '' }}>Assigned</option>
                    <option value="in_progress" {{ $request_status == 'in_progress' ? 'selected' : '' }}>In Progress</option>
                    <option value="pending" {{ $request_status == 'pending' ? 'selected' : '' }}>Pending</option>
                    <option value="approved" {{ $request_status == 'approved' ? 'selected' : '' }}>Approved</option>
                    <option value="disapproved" {{ $request_status == 'disapproved' ? 'selected' : '' }}>Disapproved</option>
                </select>
            </div>
            <div class="form-group">
                <p>Update User?</p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="update_user" id="exampleRadios1" value="1" checked>
                    <label class="form-check-label" for="exampleRadios1">
                        Yes
                    </label>
                    </div>
                    <div class="form-check">
                    <input class="form-check-input" type="radio" name="update_user" id="exampleRadios2" value="0">
                    <label class="form-check-label" for="exampleRadios2">
                        No
                    </label>
                </div>
            </div>
            <div class="form-group">
                <textarea class="form-control" 
                    placeholder="Add update info to user.." 
                    style="border-right: solid 4px #f6c23e"
                    name="update_info"
                    ></textarea>
            </div>
            <div class="form-group">
            <label for="myfile">Select a file:</label>
            <input class="form-control" type="file" name="file">
            </div>
        </div>
    </div>
    <div class="col-12" align="right">
        <div class="col-12">
            <a href="" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@push('scripts')
    <script>
        $('#post_activity_form').register_fields('.errors');
    </script>
@endpush