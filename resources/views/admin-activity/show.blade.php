<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label><b>Created At</b></label><br>
                <p>&nbsp;{{ date('F d , Y H:i A', strtotime($activity->created_at))  }}</p>
            </div>
            <div class="form-group col-md-6"></div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label><b>Subject</b></label><br>
                <p>&nbsp;{{ $activity->subject }}</p>
            </div>
            <div class="form-group col-md-6"></div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label><b>Remarks</b></label><br>
                <p>&nbsp;{{ $activity->remarks }}</p>
            </div>
            <div class="form-group col-md-6"></div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label><b>Priority</b></label><br>
                @if ($activity->priority == 'low')
                    <span class="badge badge-pill badge-light">{{ ucwords($activity->priority) }}</span>
                @elseif ($activity->priority == 'medium')
                    <span class="badge badge-pill badge-warning">{{ ucwords($activity->priority) }}</span>
                @else
                    <span class="badge badge-pill badge-danger">{{ ucwords($activity->priority) }}</span>
                @endIf
            </div>
            <div class="form-group col-md-6"></div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label><b>Assigned To</b></label><br>
                <p>&nbsp;{{ $activity->assigned_to != null ? $activity->user->name : 'N/A' }}</p>
            </div>
            <div class="form-group col-md-6"></div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label><b>Status</b></label><br>
                <p>&nbsp;{{ ucwords(str_replace('_', ' ', $activity->status)) }}</p>
            </div>
            <div class="form-group col-md-6"></div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label><b>Updated to User?</b></label><br>
                <p>&nbsp;{{ $activity->update_user == 1 ? 'Yes': 'No' }}</p>
            </div>
            <div class="form-group col-md-6"></div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label><b>Updated Information</b></label><br>
                <p>&nbsp;{{ $activity->update_info != null ? $activity->update_info : 'N/A'  }}</p>
            </div>
            <div class="form-group col-md-6"></div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label><b>File Uploaded</b></label><br>
                @if($activity->file != null)
                    <a href="{!! route('download-docs-activities', $activity->id) !!}">{{ $activity->file_name }}</a>
                @else
                    <p>&nbsp;N/A</p>
                @endIf
                
            </div>
            <div class="form-group col-md-6"></div>
        </div>
    </div>
</div>