@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Requestor</h1>
<p class="mb-4">Requestor Record</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div align='right' style="margin-top: -21px;">
            <a href="{{ route('requestor-profiles.index') }}">
                <button type="button" style="margin-bottom: -44px;" class="btn btn-default">
                <i class="fas fa-chevron-left"></i> Back</button
            ></a>
        </div>
        <h6 class="m-0 font-weight-bold text-primary">Edit Requestor Information</h6>
    </div>
    
    <div class="card-body">
        <!-- Nested Row within Card Body -->
        <div class="errors"></div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="p-5">
                    <form class="user" method="POST" id ="requestor-create" action="{{ route('requestor-profiles.update', $requestor->id) }}">
                        <input type="hidden" name="_method" value="PUT">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control form-control" id="first_name"
                                    placeholder="First Name" data-validation="required" name="first_name"
                                    style="border-right: solid 4px #f6c23e" value="{{ $requestor->first_name}}">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control form-control" id="exampleFirstName"
                                    placeholder="Middle Name" name="middle_name"
                                    style="border-right: solid 4px #f6c23e" value="{{$requestor->middle_name}}">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control form-control" id="exampleFirstName"
                                    placeholder="Last Name" data-validation="required" name="last_name"
                                    style="border-right: solid 4px #f6c23e" value="{{ $requestor->last_name }}">
                        </div>
                        <div class="form-group">
                            <input type="date" class="form-control form-control" id="birth_date"
                                    placeholder="Birth Date" data-validation="required" name="birth_date"
                                    style="border-right: solid 4px #f6c23e" value="{{ $requestor->birth_date }}">
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-control form-control" id="exampleFirstName"
                                    placeholder="Contact Number" name="contact_no"
                                    style="border-right: solid 4px #f6c23e" value="{{ $requestor->contact_no }}">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="address" 
                                placeholder="Add Address...">{{$requestor->address}}</textarea>
                        </div>
                        <div class="form-group">
                            <p>Active</p>
                            <div class="form-check">
                                <input class="form-check-input checkbox_check" type="checkbox" name="active" id="active" value="1" {{ ($requestor->active == 1?"checked":"")}}>
                                <label class="form-check-label" for="exampleRadios1" >
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div id="hotlines">
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-success btn-icon-split">
                                <span class="icon text-white-50">
                                    <i class="fas fa-edit"></i>
                                </span>
                                <span class="text">Update</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $('#requestor-create').register_fields('.errors');
    </script>
    <!-- Page level custom scripts -->
    <script src="{{ asset('web-design/js/demo/datatables-demo.js') }}"></script>
@endpush
@endsection