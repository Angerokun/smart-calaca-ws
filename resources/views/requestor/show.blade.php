@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Requestor</h1>
<p class="mb-4">Requestor Record</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div align='right' style="margin-top: -21px;">
            <a href="{{ route('requestor-profiles.index') }}">
                <button type="button" style="margin-bottom: -44px;" class="btn btn-default">
                <i class="fas fa-chevron-left"></i> Back</button
            ></a>
        </div>
        <h6 class="m-0 font-weight-bold text-primary">View Requestor Information</h6>
    </div>
    
    <div class="card-body">
        <!-- Nested Row within Card Body -->
        <div class="errors"></div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Name</b></label><br>
                        <p>&nbsp;{{ $requestor->first_name . " " . (!empty($requestor->middle_name)?$requestor->middle_name . " ":"") . $requestor->last_name}}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Birthdate</b></label><br>
                        <p>&nbsp;{{ date('F d, Y' , strtotime($requestor->birth_date) )}}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Email</b></label><br>
                        <p>&nbsp;{{ ($requestor->user->email)}}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Username</b></label><br>
                        <p>&nbsp;{{ ($requestor->user->name)}}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Contact Number</b></label><br>
                        <p>&nbsp;{{ (!empty($requestor->contact_no)?$requestor->contact_no:"N/A")}}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Address</b></label><br>
                        <p>&nbsp;{{ (!empty($requestor->address)?$requestor->address:"N/A")}}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Active</b></label><br>
                        <p>&nbsp;{{ ($requestor->active == 1?"Yes":"No")}}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection