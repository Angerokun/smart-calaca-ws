@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Administrator</h1>
<p class="mb-4">Administrator are the one who access Web Portal for SMART CALACA.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div align='right' style="margin-top: -21px;">
            <a href="{{ route('admin-profiles.index') }}">
                <button type="button" style="margin-bottom: -44px;" class="btn btn-default">
                <i class="fas fa-chevron-left"></i> Back</button
            ></a>
        </div>
        <h6 class="m-0 font-weight-bold text-primary">Create New Admin</h6>
    </div>
    
    <div class="card-body">
        <!-- Nested Row within Card Body -->
        <div class="errors"></div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="p-5">
                    <div class="text-center">
                        <h2 class="h4 text-gray-900 mb-4">Basic Information</h2>
                    </div>
                    <form class="user" method="POST" id ="admin-create" action="{{ route('admin-profiles.store') }}">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control form-control" id="exampleFirstName"
                                    placeholder="First Name" data-validation="required"
                                    style="border-right: solid 4px #f6c23e">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control form-control" id="exampleFirstName"
                                    placeholder="Middle Name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control form-control" id="exampleFirstName"
                                    placeholder="Last Name" data-validation="required"
                                    style="border-right: solid 4px #f6c23e">
                        </div>
                        <div class="form-group">
                            <select type="text" class="form-control form-control" 
                                style="border-right: solid 4px #f6c23e" data-validation="required">
                                <option value="">Select Designation</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control form-control" id="exampleFirstName"
                                    placeholder="Contact No." data-validation="required"
                                    style="border-right: solid 4px #f6c23e">
                        </div>
                        <div class="form-group">
                            <input type="date" class="form-control form-control" id="exampleFirstName"
                                    placeholder="Birth Date" data-validation="required"
                                    style="border-right: solid 4px #f6c23e">
                        </div>
                        <div class="text-center">
                            <h2 class="h4 text-gray-900 mb-4">Account Information</h2>
                        </div>
                        <hr>
                        <div class="form-group">
                            <input type="text" class="form-control form-control" id="exampleInputEmail"
                                placeholder="User Name" data-validation="required"
                                style="border-right: solid 4px #f6c23e">
                        </div>
                        <div class="form-group">
                            <select type="text" class="form-control form-control" data-validation="required" 
                            style="border-right: solid 4px #f6c23e">
                                <option value="">Select Access Control</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control form-control" id="exampleInputEmail"
                                placeholder="Email Address" data-validation="required"
                                style="border-right: solid 4px #f6c23e">
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="password" class="form-control form-control"
                                    id="exampleInputPassword" data-validation="required"
                                    placeholder="Password" style="border-right: solid 4px #f6c23e">
                            </div>
                            <div class="col-sm-6">
                                <input type="password" class="form-control"
                                    id="exampleRepeatPassword" data-validation="required"
                                     placeholder="Repeat Password" style="border-right: solid 4px #f6c23e">
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-success btn-icon-split">
                                <span class="icon text-white-50">
                                    <i class="fas fa-save"></i>
                                </span>
                                <span class="text">Submit</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $('#admin-create').register_fields('.errors');
    </script>
    <!-- Page level custom scripts -->
    <script src="{{ asset('web-design/js/demo/datatables-demo.js') }}"></script>
@endpush
@endsection