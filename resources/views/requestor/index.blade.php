@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Requestor</h1>
<p class="mb-4">Citizen Account Information Request Form Register Via Mobile App.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Requestor List</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="requestor-table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Contact No.</th>
                        <th>Birth Date</th>
                        <th>Address</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@push('scripts')
<script>
        $(function() {
            $('#requestor-table').DataTable(
                {
                    responsive: true,
                    pagingType: "full_numbers",
                    processing: true,
                    serverSide: true,
                    type: "GET",
                    ajax:"{!! route('requestor-profile-list') !!}",
                    columns:[
                        {data: "name","name":"name","width":"22%",
                            "render": function (data, type, row, meta) {
                                var url = '{{ route("requestor-profiles.show", ":id") }}';
                                url = url.replace(':id', row.id);
                                return "<a href='"+url+"'>"+data+"</a>"; 
                        }},
                        {data: "email","name":"email","width":"20%"},
                        {data: "contact_no","name":"contact_no","width":"15%"},
                        {data: "birth_date","name":"birth_date","width":"15%"},
                        {data: "address","name":"address","width":"20%"},
                        {data: "action","name":"action",orderable:false,searchable:false,"width":"8%"},
                    ]
                }
            );
        });
    </script>
    <!-- Page level custom scripts -->
    <script src="{{ asset('web-design/js/demo/datatables-demo.js') }}"></script>
@endpush
@endsection