<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
    <div class="sidebar-brand-icon rotate-n-15">
        <i class="fa fa-heartbeat"></i>
    </div>
    <div class="sidebar-brand-text mx-2" style="margin-bottom: -19px;">SMART CALACA<p class="small">Web Portal</p></div>
</a>
{!! $modules !!}
<!-- <hr class="sidebar-divider">
<div class="sidebar-heading">
    Home
</div>
<li class="nav-item">
    <a class="nav-link" href="index.html">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li>

<hr class="sidebar-divider">
<div class="sidebar-heading">
    User Management
</div>
<li class="nav-item">
    <a class="nav-link" href="index.html">
        <i class="fas fa-users-cog"></i>
        <span>Administrators</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="index.html">
        <i class="fas fa-users"></i>
        <span>Requestor</span></a>
</li>

<hr class="sidebar-divider">
<div class="sidebar-heading">
    Role Management
</div>
<li class="nav-item">
    <a class="nav-link" href="charts.html">
        <i class="fas fa-gamepad"></i>
        <span>Access Control Level</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="tables.html">
        <i class="fas fa-building"></i>
        <span>Department</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="tables.html">
        <i class="fas fa-user-md"></i>
        <span>Designation</span></a>
</li>

<hr class="sidebar-divider">
<div class="sidebar-heading">
    Role Management
</div>
<li class="nav-item">
    <a class="nav-link" href="charts.html">
        <i class="fab fa-wpforms"></i>
        <span>Request Forms</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="tables.html">
        <i class="fas fa-ambulance"></i>
        <span>Emergency Request</span></a>
</li> -->

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>