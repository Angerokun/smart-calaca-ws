@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Designation</h1>
<p class="mb-4">Specific Job title or Position per Department on the Municipality.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div align='right' style="margin-top: -21px;">
            <a href="{{ route('designations.index') }}">
                <button type="button" style="margin-bottom: -44px;" class="btn btn-default">
                <i class="fas fa-chevron-left"></i> Back</button
            ></a>
        </div>
        <h6 class="m-0 font-weight-bold text-primary">Create New Designation</h6>
    </div>
    
    <div class="card-body">
        <!-- Nested Row within Card Body -->
        <div class="errors"></div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="p-5">
                    <form class="user" method="POST" id ="designation-create" action="{{ route('designations.store') }}">
                        @csrf
                        <div class="form-group">
                            <select class="form-control" name="department_id">
                                <option value="">Select Department</option>
                                @foreach($departments as $department)
                                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control form-control" id="exampleFirstName"
                                    placeholder="Designation Name" data-validation="required" name="name"
                                    style="border-right: solid 4px #f6c23e">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="description" placeholder="Add Designation Description..."></textarea>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-success btn-icon-split">
                                <span class="icon text-white-50">
                                    <i class="fas fa-save"></i>
                                </span>
                                <span class="text">Submit</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $('#designation-create').register_fields('.errors');
    </script>
    <!-- Page level custom scripts -->
    <script src="{{ asset('web-design/js/demo/datatables-demo.js') }}"></script>
@endpush
@endsection