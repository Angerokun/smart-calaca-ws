@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Designation</h1>
<p class="mb-4">Specific Job title or Position per Department on the Municipality.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div align='right' style="margin-top: -21px;">
            <a href="{{ route('designations.index') }}">
                <button type="button" style="margin-bottom: -44px;" class="btn btn-default">
                <i class="fas fa-chevron-left"></i> Back</button
            ></a>
        </div>
        <h6 class="m-0 font-weight-bold text-primary">{{ $designation->name }}</h6>
    </div>
    
    <div class="card-body">
        <!-- Nested Row within Card Body -->
        <div class="errors"></div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Name</b></label><br>
                        <p>&nbsp;{{ $designation->name }}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Department</b></label><br>
                        <p>&nbsp;{{ $designation->department->name }}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Description</b></label><br>
                        <p>&nbsp;{{ $designation->description != null ? $designation->description : 'N/A' }}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection