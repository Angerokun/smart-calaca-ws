@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Access Controls</h1>
<p class="mb-4">User Access Controls for Municipality Officials.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div align='right' style="margin-top: -21px;">
            <a href="{{ route('access-controls.index') }}">
                <button type="button" style="margin-bottom: -44px;" class="btn btn-default">
                <i class="fas fa-chevron-left"></i> Back</button
            ></a>
        </div>
        <h6 class="m-0 font-weight-bold text-primary">Create New Access Control</h6>
    </div>
    
    <div class="card-body">
        <!-- Nested Row within Card Body -->
        <div class="errors"></div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="p-5">
                    <form class="user" method="POST" id ="access-control-create" action="{{ route('access-controls.store') }}">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control form-control"
                                    placeholder="Access Control name" data-validation="required" name="name"
                                    style="border-right: solid 4px #f6c23e">
                        </div>
                        <div class="form-group">
                            <select style="border-left: solid 2.0px #ec0000" 
                                class="form-control" placeholder="User Type"
                                name="user_type" value="" data-validation="required">
                                <option value="admin">Admin</option>
                                <option value="user">User</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="description"></textarea>
                        </div>
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table-hover text-center table-mediascreen" style="margin-top: 50px">
                                <thead class="thead-dark">
                                    <tr align="center">
                                        <th width="70%" rowspan="2" style="text-align:center" >Modules</th>
                                        <th width="30%" colspan="2" style="text-align:center" >Permissions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($modules as $module)
                                        <tr>
                                            <td>{{ $module->name }}<input type="hidden" name="modules[]" value="{{ $module->id }}"></td>
                                            <td>
                                                <select class="form-control select-picker" name="access_{{$module->id}}[]" multiple>
                                                    @foreach($restrictions as $restriction)
                                                        @php
                                                            $access = array();
                                                            $access = json_decode($module->access);
                                                        @endphp
                                                        @if (in_array($restriction->id, $access))
                                                            <option value="{{ $restriction->id }}">{{ $restriction->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-success btn-icon-split">
                                <span class="icon text-white-50">
                                    <i class="fas fa-save"></i>
                                </span>
                                <span class="text">Submit</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $('#access-control-create').register_fields('.errors');

        $('.select-picker').selectpicker();
    </script>
    <!-- Page level custom scripts -->
    <script src="{{ asset('web-design/js/demo/datatables-demo.js') }}"></script>
@endpush
@endsection