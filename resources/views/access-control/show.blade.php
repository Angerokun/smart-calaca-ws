@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Access Controls</h1>
<p class="mb-4">User Access Controls for Municipality Officials.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div align='right' style="margin-top: -21px;">
            <a href="{{ route('access-controls.index') }}">
                <button type="button" style="margin-bottom: -44px;" class="btn btn-default">
                <i class="fas fa-chevron-left"></i> Back</button
            ></a>
        </div>
        <h6 class="m-0 font-weight-bold text-primary">{{ $accessControl->name }}</h6>
    </div>
    
    <div class="card-body">
        <!-- Nested Row within Card Body -->
        <div class="errors"></div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Access Control Name</b></label><br>
                        <p>&nbsp;{{ $accessControl->name }}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>User Type</b></label><br>
                        <p>&nbsp;{{ ucwords($accessControl->user_type) }}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Description</b></label><br>
                        <p>&nbsp;{{ $accessControl->description != null ? $accessControl->description : 'N/A' }}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="col-md-12">
                    <table class="table table-striped table-bordered table-hover text-center table-mediascreen" style="margin-top: 50px">
                        <thead class="thead-dark">
                            <tr align="center">
                                <th width="50%">Modules</th>
                                <th width="50%">Permission</th>
                            </tr>
                        </thead>
                        <tbody class="report" name="report" id="table_item">
                            @foreach($accessControl->modulePermission as $module)
                                <tr align="left">
                                    <td>{{ $module->module->name }}</td>
                                    <td>{{ App\Common\restrictions($module->access) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection