@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Forms</h1>
<p class="mb-4">Forms uses from different Department.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div align='right' style="margin-top: -21px;">
            <a href="{{ route('forms.index') }}">
                <button type="button" style="margin-bottom: -44px;" class="btn btn-default">
                <i class="fas fa-chevron-left"></i> Back</button
            ></a>
        </div>
        <h6 class="m-0 font-weight-bold text-primary">Create New Forms</h6>
    </div>
    
    <div class="card-body">
        <!-- Nested Row within Card Body -->
        <div class="errors"></div>
        <form class="user" method="POST" id ="form-create" action="{{ route('forms.store') }}">
            @csrf
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="p-5">
                            <div class="form-group">
                                <select class="form-control" name="department_id" style="border-right: solid 4px #f6c23e">
                                    <option value=null>Select Department</option>
                                    @foreach($departments as $department)
                                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control form-control" id="exampleFirstName"
                                        placeholder="Form Name" data-validation="required" name="name"
                                        style="border-right: solid 4px #f6c23e">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="description" placeholder="Form Description..."></textarea>
                            </div>
  
                    </div>
                </div>
                
                <div class="col-md-12">
                <p>Add Below all needed requirements to provide by Requestor.</p>
                    <table class="table table-striped table-bordered table-hover text-center table-mediascreen" style="margin-top: 50px">
                        <thead class="thead-dark">
                            <tr align="center">
                                <th width="5%" style="text-align:center" >No.</th>
                                <th width="30%" style="text-align:center" >Requirement Name</th>
                                <th width="30%" style="text-align:center" >Digital Form</th>
                                <th width="30%" style="text-align:center" >Description</th>
                                <th width="5%" style="text-align:center" >Action</th>
                            </tr>
                        </thead>
                        <tbody id="requirement_row">
                            <tr align="center">
                                <input type="hidden" class="for_requirements" name="for_requirements[]" value="0">
                                <td class="item_no">1</td>
                                <td>
                                    <input type="text" class="form-control" 
                                    style="border-right: solid 4px #f6c23e"
                                    data-validation="required"
                                    name="name0" id="name0" placeholder="Request Form Name">
                                </td>
                                <td>
                                    <select class="form-control" name="file_id0" id="file_id0" >
                                        <option value="">Select Digital Forms</option>
                                        @foreach($digitalForms as $digitalForm)
                                            <option value="{{ $digitalForm->id }}">{{ $digitalForm->name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <textarea class="form-control" name="description0" id="description0" ></textarea>
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-7" align="left">
                            <button class="btn btn-primary addNew" type="button" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-plus-square" aria-hidden="true"></i> ADD ROW</button>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-success btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">Submit</span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@push('scripts')
    <script>
        $('#form-create').register_fields('.errors');

        var count = 1;
        var countItem = 2;
        $(".addNew").click(function(e){
            e.preventDefault();
            $('#requirement_row').append(
                '<tr align="center" class="appendRow">'+
                    '<input type="hidden" class="for_requirements" name="for_requirements[]" value="'+count+'">'+
                    '<td class="item_no">'+countItem+'</td>'+
                    '<td>'+
                        '<input type="text" class="form-control" data-validation="required" style="border-right: solid 4px #f6c23e" name="name'+count+'" id="name'+count+'" placeholder="Form Name">'+
                    '</td>'+
                    '<td>'+
                        '<select class="form-control" name="file_id'+count+'" id="file_id'+count+'" >'+
                            '<option value="">Select Digital Forms</option>'+
                            @foreach($digitalForms as $digitalForm)
                                '<option value="{{ $digitalForm->id }}">{{ $digitalForm->name }}</option>'+
                            @endforeach
                        '</select>'+
                    '</td>'+
                    '<td>'+
                        '<textarea class="form-control" name="description'+count+'" id="description'+count+'" ></textarea>'+
                    '</td>'+
                    '<td>'+
                        '<a class="btn btn-danger btn-circle btn-sm RemoveBtn">'+
                            '<i class="fas fa-trash"></i>'+
                        '</a>'+
                    '</td>'+
                '</tr>'
            );
            updateItem(); // update item number
            count++;
            countItem ++;

            $("#requirement_row").on('click','.RemoveBtn',function(e){
                e.preventDefault();
                updateItem(); // update item number
                $(this).parent().parent().remove();

            });
        });

        //update item no.
        function updateItem(){
            var rowCount = 0;
            var ctr = 1;
            var table = document.getElementById("requirement_row");
            rowCount = table.rows.length;
            $('.item_no').each( function () {
                var temp = rowCount - ctr;
                var temp2 = rowCount -temp;
                $(this).html(temp2);
                ctr++
            });
        }
    </script>
    <!-- Page level custom scripts -->
    <script src="{{ asset('web-design/js/demo/datatables-demo.js') }}"></script>
@endpush
@endsection