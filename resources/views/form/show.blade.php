@extends('layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Forms</h1>
<p class="mb-4">Forms uses from different Department.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div align='right' style="margin-top: -21px;">
            <a href="{{ route('forms.index') }}">
                <button type="button" style="margin-bottom: -44px;" class="btn btn-default">
                <i class="fas fa-chevron-left"></i> Back</button
            ></a>
        </div>
        <h6 class="m-0 font-weight-bold text-primary">{{ $form->name }}</h6>
    </div>
    
    <div class="card-body">
        <!-- Nested Row within Card Body -->
        <div class="errors"></div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Name</b></label><br>
                        <p>&nbsp;{{ $form->name }}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Department</b></label><br>
                        <p>&nbsp;{{ $form->department->name }}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Description</b></label><br>
                        <p>&nbsp;{{ $form->description != null ? $form->description : 'N/A' }}</p>
                    </div>
                    <div class="form-group col-md-6"></div>
                </div>
                <hr>
                <p>Listed Below all Requirements needed.</p>
                <div class="list-group">
                    @foreach ($form->formRequirements as $key => $requirement)
                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">{{ $requirement->name }}</h5>
                        <small><span class="badge badge-primary">{{ $key+1 }}</span></small>
                        </div>
                        <p class="mb-1">{{ $requirement->description }}</p>
                        @if ($requirement->file_id != null)
                        <small>Attached Digital Form: {{ $requirement->digitalForm->name }}</small>
                        @endIf
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection